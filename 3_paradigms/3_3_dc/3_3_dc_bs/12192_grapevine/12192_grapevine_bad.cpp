#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

typedef vector<int> vi;

#define DEBUG 1

vector<vi> v;
int n, m, l, u;

int getMinWidth()
{
   int maxStart = 0;
   int minEnd = m - 1;
   int colWidth = m;
   for (int r = 0; r < n; ++r) {
      const int s = lower_bound(begin(v[r]), end(v[r]), l) - v[r].begin();
      const int e = upper_bound(begin(v[r]), end(v[r]), u) - v[r].begin();
      const int newColWidth = max<>(0, e - s);
      if (newColWidth >)
      //maxStart = max<>(maxStart, s);
      //minEnd = min<>(minEnd, e);
   }

   const int colWidth = max<>(0, minEnd - maxStart);

   // transpose relevant columns
   vector<vi> vt;
   for (int c = maxStart; c < minEnd; ++c) {
      vi row;
      for (int r = 0; r < n; ++r) {
         row.push_back(v[r][c]);
      }
      vt.push_back(row);
   }

   maxStart = 0;
   minEnd = n - 1;
   for (int r = 0; r < colWidth; ++r) {
      const int s = lower_bound(begin(vt[r]), end(vt[r]), l) - vt[r].begin();
      const int e = upper_bound(begin(vt[r]), end(vt[r]), u) - vt[r].begin();
      maxStart = max<>(maxStart, s);
      minEnd = min<>(minEnd, e);
   }

   const int rowWidth = max<>(0, minEnd - maxStart);

   return min<>(colWidth, rowWidth);
}

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   while (!cin.eof()) {
      n = 0;
      cin >> n;
      m = 0;
      cin >> m;
      if (n == 0 || m == 0) break;

      for (int r = 0; r < n; ++r) {
         vi row(m);
         for (int c = 0; c < m; ++c) {
            cin >> row[c];
         }
         v.push_back(row);
      }

      int q = 0;
      cin >> q;
      while (q--) {
         l = 0;
         cin >> l;
         u = 0;
         cin >> u;
         if (l == 0 && u == 0) break;
         const int w = getMinWidth();
         cout << w << endl;
      }

      cout << "-" << endl;
   }

   return 0;
}