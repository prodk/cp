#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

typedef vector<int> vi;
vector<vi> vt;

//#define DEBUG 1

vector<vi> v;
int n, m, l, u;

int getMinWidth()
{
   int w = 0;
   const int high = u;
   for (int r = 0; r < n; ++r) {
      const int s = lower_bound(begin(v[r]), end(v[r]), l) - v[r].begin();
      if (s >= m) continue;
      const int e = upper_bound(begin(v[r]), end(v[r]), high) - v[r].begin();

      int colWidth = max<>(0, e - s);
      if (colWidth > w) {
         const int startCol = s;
         int st = upper_bound(vt[startCol].begin() + r, end(vt[startCol]), high) - vt[startCol].begin();

         int endCol = e - 1;
         while (colWidth > w && endCol >= 0) {
            int et = upper_bound(vt[endCol].begin() + r, end(vt[endCol]), high) - vt[endCol].begin();

            const int rowWidth = min<>(st - r, et - r);
            const int newWidth = min<>(colWidth, rowWidth);
            w = max<>(w, newWidth);
            --endCol;
            --colWidth;
         }
      }
   }

   return w;
}

int main()
{
//#ifdef DEBUG
  // freopen("in.txt", "r", stdin);
  // freopen("out.txt", "w", stdout);
//#endif

   while (!cin.eof()) {
      n = 0;
      cin >> n;
      m = 0;
      cin >> m;
      if (n == 0 || m == 0) break;

      v.clear();
      for (int r = 0; r < n; ++r) {
         vi row(m);
         for (int c = 0; c < m; ++c) {
            cin >> row[c];
         }
         v.push_back(row);
      }

      vt.clear();
      for (int c = 0; c < m; ++c) {
         vi row;
         for (int r = 0; r < n; ++r) {
            row.push_back(v[r][c]);
         }
         vt.push_back(row);
      }

      int q = 0;
      cin >> q;
      while (q--) {
         l = 0;
         cin >> l;
         u = 0;
         cin >> u;
         u; // upperbound looks for the greater values
         if (l == 0 && u == 0) break;
         const int w = getMinWidth();
         cout << w << endl;
      }

      cout << "-" << endl;
   }

   return 0;
}