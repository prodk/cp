#include <cstdio>
#include <iostream>
#include <unordered_map>
#include <algorithm>
#include <string>
#include <vector>
using namespace std;

#define DEBUG

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   string line;
   cin >> line;
   const int n = (int)line.size();

   unordered_map<char, vector<int>> m;
   for (int i = 0; i < n; ++i) {
      const char c = line[i];
      if (m.find(c) == m.end()) {
         vector<int> v;
         m[c] = v;
      }
      m[c].push_back(i);
   }

   int q = 0; cin >> q;
   while (q--) {
      string s;
      cin >> s;
      if (s.empty()) break;

      int prev = 0;
      int start = 0;
      bool valid = !m[s[0]].empty();
      if (valid) {
         prev = m[s[0]][0];
         start = prev;
         for (int i = 1; i < (int)s.size() && valid; ++i) {
            const char c = s[i];

            auto iter = lower_bound(m[c].begin(), m[c].end(), prev+1);
            if (iter == m[c].end() || *iter <= prev) valid = false;
            else {
               prev = *iter;
            }
         }
      }

      if (valid) cout << "Matched " << start << " " << prev << endl;
      else cout << "Not matched" << endl;
   }

   return 0;
}