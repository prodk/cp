#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

#define DEBUG 1
int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   while (!cin.eof()) {
      int n = 0;
      cin >> n;
      if (n == 0) break;
      vector<int> p(n);
      for (int i = 0; i < n; ++i) cin >> p[i];

      int m = 0;
      cin >> m;
      sort(begin(p), end(p));

      int lo = 0;
      int hi = n - 1;
      int rlo = p[lo];
      int rhi = p[hi];
      int prevDiff = rhi - rlo;
      while (lo < hi) {
         const int sum = p[lo] + p[hi];
         if (sum == m) {
            const int diff = p[hi] - p[lo];
            if (diff < prevDiff) {
               rlo = p[lo];
               rhi = p[hi];
            }
            ++lo;
         }
         else if (sum < m) ++lo;
         else --hi;
      }

      cout << "Peter should buy books whose prices are " << rlo << " and " << rhi << "." << endl;
      cout << endl;
   }

   return 0;
}