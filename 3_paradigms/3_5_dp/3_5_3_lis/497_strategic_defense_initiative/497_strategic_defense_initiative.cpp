#include <iostream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <vector>
#include <functional>
#include <string>
#include <sstream>
#include <stack>
using namespace std;

typedef pair<int, int> ii;

#define DEBUG

int main()
{
#ifdef DEBUG
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif

    int t = 0;
    cin >> t;
    cin.ignore();
    cin.ignore();
    /*string line;
    getline(cin, line);
    getline(cin, line);*/
    while (t--) {
        vector<int> v;
        string line;
        while (getline(cin, line) && line != "") {
            stringstream ss(line);
            int i = 0;
            ss >> i;
            v.push_back(i);
        }

        const int n = (int)v.size();
        vector<int> dp(n, 1);
        vector<int> edge(n, -1);
        for (int i = 1; i < n; ++i) {
            for (int j = 0; j < i; ++j) {
                if (v[i] > v[j]) {
                    const int newDp = dp[j] + 1;
                    if (dp[i] < newDp) {
                        dp[i] = newDp;
                        edge[i] = j;
                    }
                }
            }
        }

        const int lastId = static_cast<int>(distance(dp.begin(), max_element(begin(dp), end(dp))) );

        cout << "Max hits: " << dp[lastId] << endl;
        stack<int> hits;
        for (int i = lastId; i != -1; i = edge[i]) hits.push(i);
        while (!hits.empty()) {
            cout << v[hits.top()] << endl;
            hits.pop();
        }
        if (t != 0) cout << endl;
    }

    return 0;
}