#include <iostream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <vector>
using namespace std;

#define DEBUG
const int N = 24;

int dp[N];

int main()
{
#ifdef DEBUG
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif

    int n = 0;
    cin >> n;

    vector<int> o(n);
    for (int i = 0; i < n; ++i) {
        cin >> o[i];
    }


    while (!cin.eof()) {
        map<int, int> m;
        for (int i = 0; i < n; ++i) {
            if (cin.eof()) return 0;
            int j = 0;
            cin >> j;
            m[j] = i;
        }

        fill(dp, dp + N, 1);

        vector<int> v;
        for (int i = 1; i <= n; ++i) v.push_back(o[m[i]]);

        int res = 1;
        for (int state = 1; state < n; ++state) {
            for (int i = 0; i < state; ++i) {
                if (v[state] >= v[i]) {
                    dp[state] = max(dp[state], dp[i] + 1);
                    res = max(res, dp[state]);
                }
            }
        }

        cout << res << endl;

    }

    return 0;
}