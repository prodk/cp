#include <iostream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <vector>
#include <functional>
using namespace std;

typedef pair<int, int> ii;

#define DEBUG

int main()
{
#ifdef DEBUG
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif

    int n = 0;
    cin >> n;

    int testCase = 0;
    while (0 != n) {
        ++testCase;
        vector<pair<ii, int>> b;
        for (int i = 0; i < n; ++i) {
            vector<int> v(3);
            cin >> v[0];
            cin >> v[1];
            cin >> v[2];

            for (int j = 0; j < 3; ++j) {
                int x = j;
                int y = (x + 1) % 3;
                int z = (y + 1) % 3;
                const ii base = v[x] > v[y] ? make_pair(v[x], v[y]) : make_pair(v[y], v[x]);
                b.push_back(make_pair(base, v[z]));
            }
        }

        sort(std::begin(b), end(b), [](const pair<ii, int> & lhs, const pair<ii, int> & rhs) {
            return lhs.first.first > rhs.first.first;
        });

        const int len = (int)b.size();
        vector<int> dp(len);
        for (int i = 0; i < len; ++i) dp[i] = b[i].second;

        int res = 0;
        for (int i = 1; i < len; ++i) {
            const int h = b[i].second;
            const int x0 = b[i].first.first;
            const int y0 = b[i].first.second;
            for (int j = 0; j < i; ++j) {
                const int x = b[j].first.first;
                const int y = b[j].first.second;
                if ((x0 < x && y0 < y) || (x0 < y && y0 < x)) {
                    dp[i] = max(dp[i], dp[j] + h);
                }
                res = max(res, dp[i]);
            }
        }

        cout << "Case " << testCase << ": maximum height = " << res << endl;

        cin >> n;
    }

    return 0;
}