#include <iostream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <vector>
#include <functional>
#include <string>
#include <sstream>
#include <stack>
using namespace std;

#define DEBUG

int main()
{
#ifdef DEBUG
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif

    int t = 0;
    cin >> t;

    int tc = 0;
    while (t--) {
        ++tc;
        int n = 0;
        cin >> n;
        vector<int> h(n);
        for (int i = 0; i < n; ++i) cin >> h[i];
        vector<int> w(n);
        for (int i = 0; i < n; ++i) cin >> w[i];

        vector<int> lis(n);
        vector<int> lds(n);
        for (int i = 0; i < n; ++i) {
            lis[i] = w[i];
            lds[i] = w[i];
        }

        for (int i = 1; i < n; ++i) {
            for (int j = 0; j < i; ++j) {
                if (h[i] > h[j]) {
                    const int newLis = lis[j] + w[i];
                    if (newLis > lis[i]) {
                        lis[i] = newLis;
                    }
                }
                if (h[i] < h[j]) {
                    const int newLds = lds[j] + w[i];
                    if (newLds > lds[i]) {
                        lds[i] = newLds;
                    }
                }
            }
        }

        const int lisWidth = *max_element(begin(lis), end(lis));
        const int ldsWidth = *max_element(begin(lds), end(lds));
        cout << "Case " << tc << ". ";
        if (lisWidth >= ldsWidth) {
            cout << "Increasing (" << lisWidth << "). Decreasing (" << ldsWidth << ")." << endl;
        }
        else {
            cout << "Decreasing (" << ldsWidth << "). Increasing (" << lisWidth << ")." << endl;
        }

    }

    return 0;
}