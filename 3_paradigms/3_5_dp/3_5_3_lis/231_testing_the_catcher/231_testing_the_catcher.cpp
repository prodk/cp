#include <iostream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <vector>
using namespace std;

#define DEBUG
//const int N = 32768;

//int dp[N];

int main()
{
#ifdef DEBUG
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif

    int n = 0;
    cin >> n;

    int count = 0;
    while (n != -1) {
		if (count > 0) cout << endl;
        ++count;
        vector<int> v;
        //v.push_back(n);
        while (n != -1) {
            v.push_back(n);
            cin >> n;
        }

        //fill(dp, dp + N, 1);
        vector<int> dp(v.size(), 1);
        int res = 1;
        for (int i = 1; i < v.size(); ++i) {
            for (int j = 0; j < i; ++j) {
                if (v[i] <= v[j]) {
                    /*dp[v[i]] = max(dp[v[i]], dp[v[j]] + 1);
                    res = max(res, dp[v[i]]);*/
                    dp[i] = max(dp[i], dp[j] + 1);
                    res = max(res, dp[i]);
                }
            }
        }

        cout << "Test #" << count << ":" << endl;
        cout << "  maximum possible interceptions: " << res << endl;

        cin >> n;
    }

    return 0;
}