#include <iostream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <vector>
#include <functional>
#include <string>
#include <sstream>
#include <stack>
using namespace std;

typedef pair<int, int> ii;

#define DEBUG

int main()
{
#ifdef DEBUG
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif

    int t = 0;
    cin >> t;
    
    while (t--) {
        int n = 0;
		cin >> n;
		vector<int> v(n);
		for (int i = 0; i < n; ++i) {
			cin >> v[i];
		}
		
		vector<int> lis(n, 1);
		vector<int> lds(n, 1);
		
		for (int i = 1; i < n; ++i) {
			for (nt j = 0; j < i; ++j) {
				if (v[i] > v[j]) {
					lis[i] = max(lis[i], lis[j] + 1);
				}
				if (v[i] < v[j]) {
					lds[i] = max(lds[i], lds[j] + 1);
				}
			}
		}
		
		int res = 0;
		for (int i = 0; i < n; ++i) {
			res = max(res, lis[i] + lds[i] - 1);
		}
		
		cout << res << endl;
	}

    return 0;
}