#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;

//#define DEBUG 1

int main()
{
//#ifdef DEBUG
   //freopen("in.txt", "r", stdin);
   //freopen("out.txt", "w", stdout);
//#endif

   int n = 0;
   cin >> n;
   while (n != 0) {
	   int sum = 0;
	   int res = 0;
	   for (int i = 0; i < n; ++i) {
		   int a = 0;
		   cin >> a;
		   sum += a;
		   res = max<>(res, sum);
		   sum = max<>(sum, 0);
	   }
	   
	   if (res > 0) cout << "The maximum winning streak is " << res << "." << endl;
	   else cout << "Losing streak." << endl;
	   
	   cin >> n;
   }

   return 0;
}

