#include <iostream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <vector>
#include <functional>
#include <string>
#include <sstream>
#include <stack>
#include <numeric>  // accumulate
#include <cstdlib>  // abs(int)
#include <climits>  // INT_MAX, INT_MIN, LONG_MIN, LLONG_MAX etc.
#include <cmath>
using namespace std;

#define DEBUG
const int L = 101;
int dp[L][L][L];
int visited[L][L][L];
int l, n;
vector<int> v;
const int LEFT = 1;
const int RIGHT = 2;

int solve(int id, int leftL, int rightL) {
    if (id >= n || leftL <= 0 || rightL <= 0) return 0;

    int & res = dp[id][leftL][rightL];
    if (res != -1) return res;

    int resLeft = 0;
    if (leftL >= 0) resLeft = 1 + solve(id + 1, leftL - v[id], rightL);

    int resRight = 0;
    if (rightL >= 0) resRight = 1 + solve(id + 1, leftL, rightL - v[id]);

    res = solve(id + 1, leftL, rightL);

    const int maxTaken = max(resLeft, resRight);
    if (maxTaken > res) {
        res = maxTaken;
        if (resLeft > resRight) visited[id][leftL][rightL] = LEFT;
        else visited[id][leftL][rightL] = RIGHT;
    }
    else {
        visited[id][leftL][rightL] = 0;
    }

    return res;
}

int main()
{
#ifdef DEBUG
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif

    int t = 0;
    cin >> t;

    while (t--) {
        cin >> l;

        while (true) {
            int a = 0;
            cin >> a;
            if (a == 0) break;
            v.push_back(a/100);
        }

        for (int i = 0; i < L; ++i)
            for (int j = 0; j < L; ++j) {
                fill(dp[i][j], dp[i][j] + L, -1);
                fill(visited[i][j], visited[i][j] + L, 0);
            }
        n = v.size();
        const int res = solve(0, l, l);
        cout << res << endl;

        vector<string> sol;
        const string port = "port";
        const string star = "starboard";
        int left = l;
        int right = l;
        for (int i = 0; i < n; ++i) {
            if (visited[i][left][right] == LEFT) {
                sol.push_back(port);
                left -= v[i];
            }
            if (visited[i][left][right] == RIGHT) {
                sol.push_back(star);
                right -= v[i];
            }
        }



    }

    return 0;
}