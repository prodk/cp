#include <iostream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <vector>
#include <functional>
#include <string>
#include <sstream>
#include <stack>
#include <numeric> // accumulate
#include <cstdlib>   // abs(int)
#include <climits>  // INT_MAX, INT_MIN, LONG_MIN, LLONG_MAX etc.
using namespace std;

#define DEBUG
const int N = 102;
const int M = N * 500;
int dp[N][M];

int main()
{
#ifdef DEBUG
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif

    int t = 0;
    cin >> t;

    while (t--) {
        int n = 0;
        cin >> n;
        vector<int> v(n+1);
        for (int i = 1; i <= n; ++i) cin >> v[i];

        const int sum = accumulate(begin(v), end(v), 0);

        const int wMax = sum / 2;
        for (int w = 0; w <= wMax; ++w) {
            dp[0][w] = 0;
        }

        for (int coin = 1; coin <= n; ++coin) {
            for (int w = 0; w <= wMax; ++w) {
                if (w < v[coin]) dp[coin][w] = dp[coin - 1][w];
                else dp[coin][w] = max(dp[coin - 1][w], v[coin] + dp[coin - 1][w - v[coin]]);
            }
        }

        const int res = sum - 2 * dp[n][wMax];
        cout << res << endl;
    }

    return 0;
}