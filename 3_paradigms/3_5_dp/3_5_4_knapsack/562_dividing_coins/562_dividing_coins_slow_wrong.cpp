#include <iostream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <vector>
#include <functional>
#include <string>
#include <sstream>
#include <stack>
#include <numeric> // accumulate
#include <cstdlib>   // abs(int)
#include <climits>  // INT_MAX, INT_MIN, LONG_MIN, LLONG_MAX etc.
using namespace std;

#define DEBUG

int main()
{
#ifdef DEBUG
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif

    int t = 0;
    cin >> t;

    while (t--) {
        int n = 0;
        cin >> n;
        vector<int> v(n);
        for (int i = 0; i < n; ++i) cin >> v[i];

        const int sum = accumulate(begin(v), end(v), 0);

        vector<int> prev;
        vector<int> cur;
        prev.push_back(sum);

        int minDelta = sum;
        for (int i = 1; i < n; ++i) {
            cur.clear();
            
            for (auto s0 : prev) {
                cur.push_back(s0);
                const int sumIfTaken = s0 - v[i];
                if (sumIfTaken >= 0) {
                    //const int s1 = sum - sumIfTaken;
                    //const int delta
                    cur.push_back(sumIfTaken);
                }
            }
            /*for (int s0 = 0; s0 <= sum; ++s0) {
                if (prev[s0]) {
                    const int sumIfTaken = s0 - v[i];
                    const int sumIfNotTaken = s0;
                    cur[sumIfNotTaken] = true;
                    if (sumIfTaken >= 0) cur[sumIfTaken] = true;
                }
            }*/
            prev.swap(cur);
        }

        int res = INT_MAX;
        for (int s0 = 0; s0 <= sum; ++s0) {
            if (cur[s0]) {
                const int s1 = sum - s0;
                const int delta = abs(s0 - s1);
                res = min(delta, res);
            }
        }

        cout << res << endl;
    }

    return 0;
}