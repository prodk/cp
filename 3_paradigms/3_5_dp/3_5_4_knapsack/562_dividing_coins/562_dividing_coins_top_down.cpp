#include <iostream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <vector>
#include <functional>
#include <string>
#include <sstream>
#include <stack>
#include <numeric> // accumulate
#include <cstdlib>   // abs(int)
#include <climits>  // INT_MAX, INT_MIN, LONG_MIN, LLONG_MAX etc.
using namespace std;

#define DEBUG
const int N = 102;
int n = 0;
int sum = 0;
vector<int> v;
const int M = N * 500;
int dp[N][M];

int solve(int id, int taken)
{
    if (id > n) return abs(taken - (sum - taken));
    int & res = dp[id][taken];
    if (res != -1) return res;

    res = min(solve(id + 1, taken), solve(id + 1, taken + v[id]));

    return res;
}

int main()
{
#ifdef DEBUG
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif

    int t = 0;
    cin >> t;

    while (t--) {
        cin >> n;
        v.resize(n+1);
        for (int i = 1; i <= n; ++i) cin >> v[i];

        sum = accumulate(begin(v), end(v), 0);
        for (int i = 0; i < N; ++i) {
            fill(dp[i], dp[i] + M, -1);
        }

        const int res = solve(0, 0);
        cout << res << endl;
    }

    return 0;
}