#include <iostream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <vector>
#include <functional>
#include <string>
#include <sstream>
#include <stack>
#include <numeric>  // accumulate
#include <cstdlib>  // abs(int)
#include <climits>  // INT_MAX, INT_MIN, LONG_MIN, LLONG_MAX etc.
#include <cmath>
#include <iomanip> // setw, setprecision
#include <cstring> // memset
using namespace std;

#define DEBUG

typedef long long ll;

const int N = 30001;
const int vMax = 11;
long long dp[vMax][N];
int v[] = { 10000, 5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5 };

long long solve(int id, int money) {
    if (id >= vMax || money < 0) return 0;
    ll & res = dp[id][money];
    if (res != -1) return res;
    if (money == 0) return res = 1;

    res = solve(id, money - v[id]);
    for (int i = id + 1; i < vMax; ++i)
        res += solve(i, money - v[i]);

    return res;
}

int main()
{
#ifdef DEBUG
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif

    double n = 0.0;
    while (cin >> n) {
        double k = n * 100.0;
        const int money = static_cast<int>(round(k));
        if (money == 0) break;

        int id = 0;
        while (v[id] > money) ++id;

        memset(dp, -1, sizeof(dp));
        const long long res = solve(id, money);

        cout << setprecision(2) << fixed << setw(6) << n << setw(17) << res << endl;
    }

    return 0;
}