#include <iostream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <vector>
#include <functional>
#include <string>
#include <sstream>
#include <stack>
#include <numeric>  // accumulate
#include <cstdlib>  // abs(int)
#include <climits>  // INT_MAX, INT_MIN, LONG_MIN, LLONG_MAX etc.
#include <cmath>
#include <iomanip> // setw, setprecision
#include <cstring> // memset
using namespace std;

#define DEBUG

typedef long long ll;

const int N = 30001;
const int vMax = 11;
ll dp[N];
int v[] = { 10000, 5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5 };

int main()
{
#ifdef DEBUG
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif

    dp[0] = 1l;

    for (int i = 0; i < vMax; ++i) {
        const int last = N - v[i];
        for (int j = 0; j < last; ++j)
            dp[j + v[i]] += dp[j];
    }

    double n = 0.0;
    while (cin >> n) {
        const int money = static_cast<int>(round(n * 100.0));
        if (money == 0) break;

        cout << setprecision(2) << fixed << setw(6) << n << setw(17) << dp[money] << endl;
    }

    return 0;
}