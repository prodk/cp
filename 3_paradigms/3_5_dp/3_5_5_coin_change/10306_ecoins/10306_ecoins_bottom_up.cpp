#include <iostream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <vector>
#include <functional>
#include <string>
#include <sstream>
#include <stack>
#include <numeric>  // accumulate
#include <cstdlib>  // abs(int)
#include <climits>  // INT_MAX, INT_MIN, LONG_MIN, LLONG_MAX etc.
#include <cmath>
#include <iomanip> // setw, setprecision
#include <cstring> // memset
using namespace std;

#define DEBUG

const int N = 301;
int dp[N][N];
vector<pair<int, int>> v;
int m, s;
const int INF = 100000;

int main()
{
#ifdef DEBUG
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
#endif

    int n = 0;
    cin >> n;
    while (n--) {
        m = 0;
        s = 0;
        cin >> m >> s;
        v.clear();
        for (int i = 0; i < m; ++i) {
            int value = 0;
            int evalue = 0;
            cin >> value >> evalue;
            v.push_back(make_pair(value, evalue));
        }

        for (int i = 0; i < N; ++i)
            fill(dp[i], dp[i]+N, INF);

        dp[0][0] = 0;
        for (int i = 0; i < m; ++i) {
            for (int j = v[i].first; j <= s; ++j) {
                for (int k = v[i].second; k <= s; ++k) {
                    dp[j][k] = min(dp[j][k], 1 + dp[j - v[i].first][k - v[i].second]);
                }
            }
        }

        int res = INF;
        for (int i = 0; i <= s; ++i)
            for (int j = 0; j <= s; ++j)
                if (i*i + j*j == s*s)
                    res = min(res, dp[i][j]);
        if (res == INF) cout << "not possible" << endl;
        else cout << res << endl;
    }

    return 0;
}