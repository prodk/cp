#include <iostream>
#include <cstdio>
#include <vector>
using namespace std;

typedef long long ll;

#define DEBUG

int in[2];

ll res;
vector<int> a;

void backtrack(int i, ll s5)
{
   if (i == 12) {
      ll s = 0l;
	  for (auto j : a) s += j;
	  if (s > res) res = s;
   }
   else {
      const int s4 = s5 - a[i-5];
	  for (int j = 0; j < 2; ++j) {
	     const int sNext = s4 + in[j];
		 if (sNext <= 0) {
		    a.push_back(in[j]);
			backtrack(i + 1, sNext);
			a.pop_back();
		 }
	  }
   }
}

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif
   
   while (!cin.eof()) {
      std::fill(in.begin(), in.end(), 0);
	  cin >> in[0];
	  if (in[0] == 0) break;
	  cin >> in[1];
	  in[1] *= -1;
	  
	  res = 0;
	  for (int i = 0; i < (1 << 5); ++i) {
	     a.clear();
		 for (int j = 0; j < 5; ++j) {
		    a.push_back(in[(i >> j)&1]);
		 }
		 
		 long long sum = 0l;
		 for (int j = 0; j < a.size(); ++j) sum += a[j];
		 if (sum <= 0) {
		    backtrack(5, sum);
		 }
	  }

      if (res > 0) cout << res << endl;
      else cout << "Deficit" << endl;	  
   }

   return 0;
}