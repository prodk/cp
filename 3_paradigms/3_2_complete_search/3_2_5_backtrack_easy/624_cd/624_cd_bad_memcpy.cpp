#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
using namespace std;

#define DEBUG 1

const int tmax = 24;

int N, t, res;
int cur[tmax], opt[24], d[24];

void backtrack(int s, int p)
{
   if (s > res) {
      res = s;
      memcpy(opt, cur, tmax);
   }

   for (int i = p; i < t; ++i) {
      const int sNext = s + d[i];
      if (sNext <= N) {
         cur[i] = d[i];
         backtrack(sNext, i + 1);
         cur[i] = 0;
      }
   }
}

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   while (!cin.eof()) {
      N = 0;
      cin >> N;
      if (N == 0) break;
      t = 0;
      cin >> t;
      for (int i = 0; i < t; ++i) {
         cin >> d[i];
      }

      res = 0;
      memset(opt, 0, tmax);
      backtrack(0, 0);
      int i = 0;
      while (i < t) {
         if (opt[i] != 0) cout << opt[i] << " ";
         ++i;
      }
      cout << "sum:" << res << endl;
   }

   return 0;
}