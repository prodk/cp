#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <vector>
using namespace std;

#define DEBUG 1

const int tmax = 24;

int N, t, res;
vector<int> cur, opt;
int d[tmax];

void backtrack(int s, int p)
{
   if (s > res) {
      res = s;
      opt = cur;
   }

   for (int i = p; i < t; ++i) {
      const int sNext = s + d[i];
      if (sNext <= N) {
         cur.push_back(d[i]);
         backtrack(sNext, i + 1);
         cur.pop_back();
      }
   }
}

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   while (!cin.eof()) {
      N = 0;
      cin >> N;
      if (N == 0) break;
      t = 0;
      cin >> t;
      for (int i = 0; i < t; ++i) {
         cin >> d[i];
      }

      res = 0;
      backtrack(0, 0);
      for (auto i : opt) cout << i << " ";
      cout << "sum:" << res << endl;
   }

   return 0;
}