#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
using namespace std;

#define DEBUG 1

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif
   int t = 0;
   cin >> t;
   while (t--) {
      int d = 0;
      cin >> d;

      vector<string> vs;
      vector<pair<int, int>> vp;
      for (int i = 0; i < d; ++i) {
         string s;
         cin >> s;
         vs.push_back(s);
         int l = 0;
         cin >> l;
         int h = 0;
         cin >> h;
         vp.push_back(make_pair(l, h));
      }

      int q = 0;
      cin >> q;
      vector<int> p(q);
      for (int i = 0; i < q; ++i) cin >> p[i];

      for (int i = 0; i < q; ++i) {
         const int price = p[i];
         int count = 0;
         string res;
         for (int j = 0; j < d; ++j) {
            if (vp[j].first <= price && price <= vp[j].second) {
               ++count;
               if (count > 1) break;
               res = vs[j];
            }
            if (count > 1) break;
         }
         if (count != 1) cout << "UNDETERMINED" << endl;
         else cout << res << endl;
      }
      if (t) cout << endl;
   }

   return 0;
}