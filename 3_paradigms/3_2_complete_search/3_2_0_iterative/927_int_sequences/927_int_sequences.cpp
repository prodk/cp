#include <iostream>
#include <cmath>
#include <cstdio>
#include <vector>
using namespace std;

#define DEBUG 1

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   int t = 0;
   cin >> t;
   while (t--)
   {
      int deg = 0;
      cin >> deg;
      vector<int> c(deg + 1);
      for (int i = 0; i < deg + 1; ++i) cin >> c[i];

      int d = 0;
      cin >> d;
      int k = 0;
      cin >> k;

      const int n = ceil(sqrt((2.0*k) / (double)d));
      int next = d*(n - 1)*n / 2;
      for (int i = n-1; i <= n+1; ++i) {
         const int cur = d*i*(i + 1) / 2;
         if (k <= cur) {
            long long an = c[0];
            long long curPow = i;
            for (int j = 1; j < c.size(); ++j) {
               an += c[j] * curPow;
               curPow *= i;
            }
            cout << an << endl;
            break;
         }
      }
   }

   return 0;
}