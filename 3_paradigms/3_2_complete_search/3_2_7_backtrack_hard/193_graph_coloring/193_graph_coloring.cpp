#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

typedef vector<int> vi;

#define DEBUG

vector<vi> g;
vi color;
vi cur;
vi sol;
int res;
static int n;
static int k;

const int none = -1;
const int black = 0;
const int white = 1;

bool hasBlackNeighbors(const int v)
{
   if (v >= n || v < 0)
      return false;

   for (auto u : g[v]) {
      if (color[u] == black)
         return true;
   }
   return false;
}

void backtrack(int v)
{
   if (v == n) {
      if (cur.size() > res || cur.size() == res && color[v - 1] == black) {
         res = cur.size();
         sol = cur;
      }
      return;
   }

   if (v < n) {
      if (!hasBlackNeighbors(v)) {
         cur.push_back(v);
         color[v] = black;
         backtrack(v + 1);
         cur.pop_back();
         color[v] = -1;
      }
      color[v] = white;
      backtrack(v + 1);
      color[v] = -1;
   }
}

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   int t = 0;
   cin >> t;
   while (t--) {
      n = 0;
      k = 0;
      cin >> n;
      cin >> k;
      if (!n) break;

      g.clear();
      g.resize(n);
      for (int i = 0; i < k; ++i) {
         int u = 0, v = 0;
         scanf("%d %d\n", &u, &v);
         g[u - 1].push_back(v - 1);
         g[v - 1].push_back(u - 1);
      }

      sol.clear();
      color.clear();
      color.resize(n);
      fill(begin(color), end(color), none);
      res = 0;
      backtrack(0);

      cout << res << endl;
      for (int i = 0; i < res; ++i) {
         cout << sol[i] + 1;
         if (i != res - 1) cout << " ";
      }
      cout << endl;
   }

   return 0;
}