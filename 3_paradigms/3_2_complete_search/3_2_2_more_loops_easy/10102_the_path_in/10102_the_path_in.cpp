#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <cstdlib>
#include <algorithm>
using namespace std;

#define DEBUG 1

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   while (!cin.eof()) {
      int n = 0;
      cin >> n;

      string line;
      vector<pair<int, int>> o;
      vector<pair<int, int>> t;
      for (int r = 0; r < n; ++r) {
         cin >> line;

         for (int c = 0; c < n; ++c) {
            if (line[c] == '1') o.push_back(make_pair(r, c));
            if (line[c] == '3') t.push_back(make_pair(r, c));
         }
      }

      int res = 0;
      for (int i = 0; i < (int)o.size(); ++i) {
         int manhat = 2 * n;
         for (int j = 0; j < (int)t.size(); ++j) {
            const int cur = abs(o[i].first - t[j].first) + abs(o[i].second - t[j].second);
            manhat = min<>(manhat, cur);
         }

         res = max<>(res, manhat);
      }

      cout << res << endl;
   }

   return 0;
}