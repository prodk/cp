#include <iostream>
#include <cstdio>
#include <vector>
using namespace std;

#define DEBUG 1
int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   //freopen("out.txt", "w", stdout);
#endif

   const char * combiPhrase = "NUMBER OF COMBINATIONS THAT SCORES ";
   const char * permuPhrase = "NUMBER OF PERMUTATIONS THAT SCORES ";
   const char * asterisc = "**********************************************************************";

   int n = 0;
   cin >> n;

   while (n > 0) {
      vector<int> combi;
      for (int i = 0; i <= n; ++i) {
         for (int j = i; j <= n; ++j) {
            for (int k = j; k <= n; ++k) {
               if (i + j + k == n) {
                  int eq = 0;
                  if (i == j) ++eq;
                  if (j == k) ++eq;
                  if (i == k) ++eq;
                  combi.push_back(eq);
               }
            }
         }
      }

      const int c = combi.size();
      if (c != 0)
      {
         int p = 0;
         for (int i = 0; i < c; ++i) {
            switch (combi[i]) {
            case 0:
               p += 6;
               break;
            case 1:
            case 2:
               p += 3;
               break;
            case 3:
               p += 1;
               break;
            }
         }

         cout << combiPhrase << n << " IS " << c << "." << endl;
         cout << permuPhrase << n << " IS " << p << "." << endl;
      }
      else {
         cout << "THE SCORE OF " << n << " CANNOT BE MADE WITH THREE DARTS." << endl;
      }

      cin >> n;
   }

   return 0;
}