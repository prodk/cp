#include <vector>
#include <iostream>
#include <cstdio>
using namespace std;

typedef long long ll;

#define DEBUG 1

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   int t = 0;
   cin >> t;
   while (t--) {
      int n = 0;
      cin >> n;
      vector<int> a(n);
      for (int i = 0; i < n; ++i) cin >> a[i];

      vector<int> b;
      for (int i = 1; i < n; ++i) {
         int count = 0;
         for (int j = 0; j < i; ++j) {
            if (a[j] <= a[i]) ++count;
         }
         b.push_back(count);
      }

      ll res = 0l;
      for (int i = 0; i < b.size(); ++i) {
         res += b[i];
      }

      cout << res << endl;
   }

   return 0;
}