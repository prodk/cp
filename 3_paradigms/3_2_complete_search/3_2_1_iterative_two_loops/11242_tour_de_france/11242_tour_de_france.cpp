#include <vector>
#include <iostream>
#include <cstdio>
#include <algorithm>
#include <iomanip>
using namespace std;

typedef long long ll;

#define DEBUG 1

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   while (!cin.eof()) {
      int f = 0;
      cin >> f;
      if (f == 0) break;
      int r = 0;
      cin >> r;

      //cout << f << " " << r << endl;

      vector<int> m(f);
      vector<int> n(r);
      for (int i = 0; i < f; ++i) cin >> m[i];
      for (int i = 0; i < r; ++i) cin >> n[i];

      vector<double> d;
      for (int i = 0; i < (int)m.size(); ++i) {
         for (int j = 0; j < (int)n.size(); ++j) {
            const double ratio = (double)n[j] / (double)m[i];
            d.push_back(ratio);
         }
      }
      sort(begin(d), end(d));

      double res = 0.0;
      for (int i = 1; i < (int)d.size(); ++i) {
         const double s = d[i] / d[i - 1];
         res = max<>(res, s);
      }

      cout << setprecision(2) << fixed << res << endl;
   }

   return 0;
}