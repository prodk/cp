#include <vector>
#include <iostream>
#include <cstdio>
#include <utility>
#include <set>
using namespace std;

#define DEBUG

typedef pair<int, int> ii;
typedef vector<ii> vii;

int n, m;
ii s, e;
vii v;
vii sol;
bool yes;

void backtrack(set<int> & used, const ii & prev) {
   if (!yes && used.size() < m) {
      if (sol.size() == n && prev.second == e.first) {
         yes = true;
      }
      else {
         for (int i = 0; i < m; ++i) {
            if (used.find(i) == used.end()) {
               used.insert(i);
               if (prev.second == v[i].first) {
                  sol.push_back(v[i]);
                  backtrack(used, v[i]);
                  sol.pop_back();
               }
               if (!yes && prev.second == v[i].second) {
                  ii next = make_pair(v[i].second, v[i].first);
                  sol.push_back(next);
                  backtrack(used, next);
                  sol.pop_back();
               }
               used.erase(i);
            }
         }
      }
   }
}

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   while (!cin.eof()) {
      n = 0;
      cin >> n;
      m = 0;
      cin >> m;
      if (n == 0 || m == 0) break;

      int p = 0;
      cin >> p;
      int q = 0;
      cin >> q;

      s = make_pair(p, q);

      cin >> p;
      cin >> q;
      e = make_pair(p, q);
      v.clear();
      for (int i = 0; i < m; ++i) {
         cin >> p; cin >> q;
         v.push_back(make_pair(p, q));
      }

      yes = false;
      set<int> used;
      backtrack(used, s);

      if (yes) cout << "YES" << endl;
      else cout << "NO" << endl;
   }

   return 0;
}