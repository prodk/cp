#include <vector>
#include <iostream>
#include <cstdio>
#include <utility>
#include <algorithm>
using namespace std;

#define DEBUG

typedef pair<int, int> ii;
typedef vector<ii> vii;

int n, m;
ii s, e;
vii v;
bool yes;
int used;

bool backtrack(int p, const ii & prev) {
   if (p == n) {
      if (prev.second == e.first) return true;
      return false;
   }

   for (int i = 0; i < m; ++i) {
      if ((used >> 1) & 1) continue;
      ii next;
      bool eq = false;
      if (prev.second == v[i].first) {
         next = v[i];
         eq = true;
      }
      else if (prev.second == v[i].second) {
         next = make_pair(v[i].second, v[i].first);
         eq = true;
      }

      if (eq) {
         used |= (1 << i);
         bool ok = backtrack(p + 1, next);
         if (ok) return ok;
         used &= ~(1 << i);
      }
   }
   return false;
}

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   while (scanf("%d\n%d", &n, &m), n != 0) {
      if (n == 0 || m == 0) break;
      int p = 0;
      int q = 0;
      scanf("%d %d", &p, &q);
      s = make_pair(p, q);

      scanf("%d %d", &p, &q);
      e = make_pair(p, q);
      v.clear();
      for (int i = 0; i < m; ++i) {
         scanf("%d %d", &p, &q);
         v.push_back(make_pair(p, q));
      }

      yes = false;
      used = 0;
      yes = backtrack(0, s);

      if (yes) puts("YES");// << endl;
      else puts("NO"); //cout << "NO" << endl;
   }

   return 0;
}