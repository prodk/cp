#include <vector>
#include <iostream>
#include <cstdio>
using namespace std;

typedef long long ll;

vector<int> in;
vector<int> old;
vector<int> cur;
int t, n, cnt;

#define DEBUG

void backtrack(int p, int s)
{
   if (s == t) {
      if (old != cur) {
         ++cnt;
         old = cur;
         const int len = (int)cur.size();
         if (len == 1) cout << cur[0] << endl;
         else {
            const int end = len - 1;
            for (int i = 0; i < len; ++i) {
               cout << cur[i];
               if (i != end) cout << "+";
            }
            cout << endl;
         }
      }
   }
   else {
      int i = p;
      while (i < n) {
         const int sNext = s + in[i];
         if (sNext <= t) {
            cur.push_back(in[i]);
            backtrack(i + 1, sNext);
            cur.pop_back();
         }
         ++i;
      }
   }
}

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif
   while (!cin.eof()) {
      cin >> t;
      if (t == 0) break;
      cin >> n;
      in.resize(n);
      for (int i = 0; i < n; ++i) cin >> in[i];

      cnt = 0;
      old.clear();
      cout << "Sums of " << t << ":" << endl;
      backtrack(0, 0);
      if (cnt == 0) cout << "NONE" << endl;
   }

   return 0;
}