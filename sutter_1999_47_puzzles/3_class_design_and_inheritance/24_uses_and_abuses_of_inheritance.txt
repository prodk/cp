Item 24. Uses and Abuses of Inheritance
Difficulty: 6

Given that
inheritance is nearly the strongest relationship you can express in C++ (second only to friendship), it's only
really appropriate when there is no equivalent weaker alternative.

template<typename T>
class MyList
{
public:
   bool insert(const T &, const size_t index);
   T access(const size_t index) const;
   size_t size() const;
private:
   T * buf_;
   size_t bufsize_;
};

Two ways to write MySet class in terms of MyList<T>:
1) 
template<typename T>
class MySet : private MyList<T>
{
public:
   bool add(const T &); // invokes bool insert()
   T get(const size_t index) const;        // uses access()
   using MyList<T>::size;
};

2)
template<typename T>
class MyList
{
public:
   bool add(const T &); // invokes bool mImpl_.insert();
   T get(const size_t index) const; // mImpl_.access();
   size_t size() const;  // return mImpl.size();
private:
   MyList<T> mImpl_;   
};

Give these alternatives some thought, and consider the following questions.
1. Is there any difference between MySet1 and MySet2?
2. More generally, what is the difference between nonpublic inheritance and containment? Make as
comprehensive a list as you can of reasons why you would use inheritance instead of containment.
3. Which version of MySet would you prefer�MySet1 or MySet2?
4. Finally, make as comprehensive a list as you can of reasons why you would use public inheritance.

______________
My sol.
1. Yes, there is a difference: for the private inheritance version, if MyList changes somehow then we
will have to recompile MyList.cpp too because in order to inherit we need to include "MyList.hpp" into "MySet.cpp".
For mImpl_ version we recompile MySet.cpp only if the interface changes.

2. The difference is that we become dependent on the class we are inheriting from.
A reason to use private inheritance is to avoid using mImpl_ all the time and just invoke the methods of the base class.

3. I would prefer using composition to avoid redundant recompilation.

4. Reasons to use public inheritance:
1) to use the same interface for different types of objects: for example if we have the same action that can be done for different objects.

______________
Their sol.
1. No, no difference.
2. Nonpublic inheritance should always express IS-IMPLEMENTED-IN-TERMS-OF (with only one rare
exception, which I'll cover shortly). It makes the using class depend upon the public AND protected
parts of the used class.
Containment always expresses HAS-A and, therefore, IS-IMPLEMENTED-IN-TERMS-OF. It makes the
using class depend upon only the public parts of the used class.

there's nothing we can do with
a single MyList<T> member that we couldn't do if we inherited from MyList<T>. Of course, using
inheritance does limit us to having just one MyList<T> (as a base subobject).

Prefer containment (a.k.a. "composition", "layering", "HAS-A", "delegation")
to inheritance. When modeling IS-IMPLEMENTED-IN-TERMS-OF, always prefer
expressing it using containment, not inheritance.
3.
4. Only use public inheritance to model true IS-A, as per the Liskov
Substitution Principle.[11] That is, a publicly derived class object should be able to be used in any context in
which the base class object could be used and still guarantee the same semantics. [Note: We covered a rare
exception�or, more correctly, an extension�to this idea in Item 3.

the key precept of public inheritance: "Require no more and
promise no less."

Always ensure that public inheritance models both IS-A and WORKS-LIKE-A
according to the Liskov Substitution Principle. All overridden member
functions must require no more and promise no less.

Never inherit publicly to reuse (code in the base class); inherit publicly in
order to be reused (by existing code that uses base objects polymorphically).




