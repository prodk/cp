Item 4. Maximally Reusable Generic Containers—Part 1
Difficulty: 8

How can you best implement copy construction and copy assignment for the following fixed-length vector
class? How can you provide maximum usability for construction and assignment? Hint: Think about the kinds
of things that client code might want to do.

________________
My Sol.

template<typename T, size_t size>
class fixed_vector
{
public:
   typedef T *       iterator;
   typedef const T * const_iterator;
   iterator          begin() { return v_; }
   iterator          end()   { return v_ + size; }
   const_iterator    begin() { return v_; }
   const_iterator    end()   { return v_ + size; }
   
   fixed_vector(const fixed_vector & rhs);
   fixed_vector & operator=(const fixed_vector & rhs);
private:
   T v_[size];
};

fixed_vector::fixed_vector(const fixed_vector & rhs)
{
   memcpy(v_, rhs.begin(), size * sizeof(T));
}

fixe_vector & fixed_vector(const fixed_vector & rhs)
{
   memcpy(v_, rhs.begin(), size * sizeof(T));
   return *this;
}

________________
Their Sol.

