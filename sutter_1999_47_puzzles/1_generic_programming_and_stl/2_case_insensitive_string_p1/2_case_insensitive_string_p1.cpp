#include <iostream>
#include <cassert>
#include <string>
#include <algorithm>
#include <locale>

class ci_string
{
public:
    ci_string(const char * str);
    ci_string(const std::string & str);

    //ci_string & ci_string(const ci_string & str);	
    //ci_string & operator=(const ci_string & str);

    bool operator==(const std::string & rhs) const;

    const char * c_str() const;

private:
    std::string mStr;
};

ci_string::ci_string(const char * str)
    : mStr(str)
{
}

ci_string::ci_string(const std::string & str)
    : mStr(str)
{
}

bool ci_string::operator==(const std::string & rhs) const
{
    const auto len = mStr.size();
    bool result = len == rhs.size();
    if (result) {
        for (auto i = 0u; i < len && result; ++i) {
            result = ::tolower(mStr[i]) == ::tolower(rhs[i]);
        }
    }

    return result;
}

const char * ci_string::c_str() const
{
    return mStr.c_str();
}

int main()
{
    ci_string s("AbCdE");
    // case insensitive

    assert(s == "abcde");
    assert(s == "ABCDE");// still case-preserving, of course

    assert(strcmp(s.c_str(), "AbCdE") == 0);
    assert(strcmp(s.c_str(), "abcde") != 0);

    return 0;
}