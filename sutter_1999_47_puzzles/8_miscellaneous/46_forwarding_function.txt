Item 46. Forwarding Functions
Difficulty: 3
What's the best way to write a forwarding function? The basic answer is easy, but we'll also learn about a
subtle change to the language made shortly before the standard was finalized.

// file f.cpp
//
#include "f.h"
/*...*/
bool f( X x )
{
return g( x );
}

___________
My sol.
bool f(const X & x)
{
   return g(x);
}
___________
Their sol.

Prefer passing objects by reference instead of by value, using const wherever possible.

Avoid the "dusty corners" of a language; use the simplest techniques that are effective.

Avoid inlining or detailed tuning until performance profiles prove the need.

