Item 39. Automatic Conversions
Difficulty: 4

It is often useful to be able to access a string as a C-style const char*. Indeed, string has a member
function c_str() to allow just that, by giving access to a const char*. Here's the difference in client
code:
string s1("hello"), s2("world");
strcmp( s1, s2 ); // 1 (error)
strcmp( s1.c_str(), s2.c_str() ) // 2 (ok)
It would certainly be nice to do #1, but #1 is an error because strcmp requires two pointers and there's no
automatic conversion from string to const char*. Number 2 is okay, but it's longer to write because we
have to call c_str() explicitly.
So this Item's question really boils down to: Wouldn't it be better if we could just write #1?
______________
My sol.
To be possible to write #1 we need to convert objects to pointers.
Say we do this by implementing const char * string::operator();
not in all the cases we want this operator return const char *.
______________
Their sol.
The answer is: No, with good reason.
It's almost always a good idea to avoid writing automatic conversions, either as conversion operators or as
single-argument non-explicit constructors.

The main reasons that implicit conversions are unsafe in general are:
Implicit conversions can interfere with overload resolution.
Implicit conversions can silently let "wrong" code compile cleanly.

Avoid writing conversion operators. Avoid non-explicit constructors.



