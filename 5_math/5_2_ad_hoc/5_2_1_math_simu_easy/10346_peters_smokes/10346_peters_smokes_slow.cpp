#include <iostream>
#include <cstdio>
using namespace std;

int n, k;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    while (cin >> n >> k && (n || k)) {
        int sum = n;
        int rest = n;
        while (rest >= k) {
            rest -= k;
            ++rest;
            ++sum;
        }
        cout << sum << endl;
    }

    return 0;
}