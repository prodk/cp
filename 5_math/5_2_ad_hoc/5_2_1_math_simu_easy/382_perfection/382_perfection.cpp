#include <iostream>
#include <cstdio>
#include <iomanip>
using namespace std;

int n;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    cout << "PERFECTION OUTPUT" << endl;

    while (cin >> n && n) {
        int sum = 0;
        for (int i = 2; i < n; ++i) {
            if (n % i == 0) sum += i;
        }
        sum += 1;

        cout << setw(5) << n << "  ";

        if (sum == n && n != 1) cout << "PERFECT" << endl;
        else if (sum < n || n == 1) cout << "DEFICIENT" << endl;
        else cout << "ABUNDANT" << endl;
    }

    cout << "END OF OUTPUT" << endl;

    return 0;
}