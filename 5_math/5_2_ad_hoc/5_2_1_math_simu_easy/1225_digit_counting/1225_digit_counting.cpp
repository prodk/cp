#include <iostream>
#include <cstdio>
#include <string>
using namespace std;

int t, n;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    cin >> t;
    while (t--) {
        cin >> n;
        int hist[10] = {};
        for (int i = 1; i <= n; ++i) {
            const string s = to_string(i);
            for (auto c : s) ++hist[c - '0'];
        }

        for (int i = 0; i < 10; ++i) {
            cout << hist[i];
            if (i != 9) cout << " ";
        }
        cout << endl;
    }

    return 0;
}