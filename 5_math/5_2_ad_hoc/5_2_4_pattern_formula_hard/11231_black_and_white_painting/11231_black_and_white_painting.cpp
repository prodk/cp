#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;

typedef long long ll;
int n, m, c;

ll getVal(const int n)
{
    return (n / 8 + (n - 2) / 8 + (n - 4) / 8 + (n - 6) / 8);
}

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    while (cin >> n >> m >> c && (n || m)) {
        ll res = 0;
        if (c == 0) {
            const ll xTop = getVal(n - 1);
            const ll yTop = getVal(m);
            const ll resTop =xTop * yTop;

            const ll xBot = getVal(n);
            const ll yBot = getVal(m - 1);
            const ll resBot = xBot * yBot;

            res = resTop + resBot;
        }
        else if (c == 1) {
            const ll x = getVal(n);
            const ll y = getVal(m);
            res = x * y;
            const ll xDiag = getVal(n - 1);
            const ll yDiag = getVal(m - 1);
            res += xDiag * yDiag;
        }

        cout << res << endl;
    }

    return 0;
}