#include <iostream>
#include <cstdio>
#include <cmath>
using namespace std;

int n;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);

    while (cin >> n && n) {
        const int layer = ceil(sqrt((float)n));
        const int delta = layer*layer - n + 1;

        // Default values for delta == layer
        int x = layer;
        int y = layer;

        if (delta < layer) {
            x = (layer % 2 == 0) ? layer : delta;
            y = (layer % 2 == 0) ? delta : layer;
        }
        else if (delta > layer) {
            const int val = 2 * layer - delta;
            x = (layer % 2 == 0) ? val : layer;
            y = (layer % 2 == 0) ? layer : val;
        }

        cout << x << " " << y << endl;
    }

    return 0;
}