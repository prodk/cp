#include <iostream>
#include <cstdio>
#include <cmath>
using namespace std;

int r, n;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    int test = 0;
    while (cin >> r >> n && (r && n)) {
        ++test;
        cout << "Case " << test << ": ";
        const int res = ceil((float)(r - n) / (float)n);
        if (res > 26) cout << "impossible" << endl;
        else cout << res << endl;
    }

    return 0;
}