#include <iostream>
#include <vector>
#include <cstdio>
using namespace std;

int t, n;

int main()
{
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
	ios_base::sync_with_stdio(0);
	
	cin >> t;
	int test = 0;
	while (t--) {
		++test;
		cin >> n;
		vector<int> v(n);
		for (int i = 0; i < n; ++i) {
			cin >> v[i];
		}
		cout << "Case " << test << ": " << v[i/2] << endl;
	}
	
	return 0;
}
