#include <iostream>
#include <cmath>
#include <cstdio>
#include <iomanip>
using namespace std;

int t;
double d, v, u;

int main()
{
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
    //ios_base::sync_with_stdio(0);

    cin >> t;
    int testCount = 0;
    while (t--) {
        ++testCount;
        cin >> d >> v >> u;

        cout << "Case " << testCount << ": ";
        const bool notOK = (u == 0.0) || (v == 0.0) || (u <= v);
        if (!notOK) {
            const double res = (d/sqrt(u*u - v*v) - d/ u);
            //cout << res;
            printf("%.3lf", res);
        }
        else {
            cout << "can't determine";
        }

        if (t) cout << endl;
    }

    return 0;
}