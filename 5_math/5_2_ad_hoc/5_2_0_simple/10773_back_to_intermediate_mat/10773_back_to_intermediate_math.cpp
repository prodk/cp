#include <iostream>
#include <cmath>
#include <cstdio>
#include <iomanip>
using namespace std;

int t;
double d, v, u;

int main()
{
   // freopen("in.txt", "r", stdin);
   // freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);

    cin >> t;
    int testCount = 0;
    while (t--) {
        ++testCount;
        cin >> d >> v >> u;

        cout << "Case " << testCount << ": ";
        const bool isOK = (u > 0.0) && (v > 0.0) && (d > 0.0) && (u > v);
        if (isOK) {
            const double res = d * (1.0 / sqrt(u*u - v*v) - 1.0 / u);
            cout << setprecision(3) << fixed << res << endl;
        }
        else {
            cout << "can\'t determine" << endl;
        }
    }

    return 0;
}