#include <stdio.h>
#include <cmath>
#include <algorithm>
using namespace std;

int main() {
    // freopen("in.txt", "r", stdin);
    // freopen("out.txt", "w", stdout);
    int n;
    while (scanf("%d", &n) == 1) {
        int layer = max(1.0, sqrt(2.0 * (n - 1)) - 2.0);
        long long vMax = 0;
        for (int i = layer; ; ++i) {
            const long long vMin = i * (i - 1) / 2 + 1;
            vMax = vMin + i - 1;
            if (n <= vMax) {
                layer = i;
                break;
            }
        }

        printf("TERM %d IS ", n);
        if (layer & 1)
            printf("%d/%d\n", 1 + vMax - n, layer - (vMax - n));
        else
            printf("%d/%d\n", layer - (vMax - n), 1 + vMax - n);
    }
    return 0;
}