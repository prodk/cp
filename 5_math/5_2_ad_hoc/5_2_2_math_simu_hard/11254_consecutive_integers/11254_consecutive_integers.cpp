#include <iostream>
#include <cstdio>
#include <cmath>
using namespace std;

int n;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    while (cin >> n && n >= 0) {
        int a = 0;
        int res = 0;
        const int nMax = ceil(sqrt(2.0 * n));
        for (int r = nMax; r >= 1; --r) {
            const int nom = (2 * n - r*(r - 1));
            const int denom = 2 * r;
            if (nom != 0 && (nom % denom == 0) && a <= n) {
                a = nom / denom;
                res = r;
                break;
            }
        }

        cout << n << " = " << a << " + ... + " << a + res - 1 << endl;
    }

    return 0;
}