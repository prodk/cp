#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
using namespace std;

int n;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    while (cin >> n && n >= 0) {
        const int nMax = ceil(sqrt((double)n));
        int res = 0;
        bool found = false;
        for (int m = 2; m <= nMax; ++m) {
            int cur = n;
            bool canBeFound = true;
            for (int i = 0; i < m && canBeFound; ++i) {
                const int rem = cur % m;
                if (rem != 1) {
                    canBeFound = false;
                }
                cur = cur - (cur / m) - rem;
            }
            if (canBeFound && cur % m == 0) {
                found = true;
                res = max(res, m);
            }
        }

        cout << n << " coconuts, ";
        if (found) cout << res << " people and 1 monkey" << endl;
        else cout << "no solution" << endl;
    }

    return 0;
}