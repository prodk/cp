#include <iostream>
#include <vector>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <string>
using namespace std;

int n;
const string index[] = { "st", "nd", "rd" };

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    const int SIZE = 5842;
    int h[SIZE];
    int n2 = 0, n3 = 0, n5 = 0, n7 = 0;
    h[0] = 1;

    for (int i = 1; i < SIZE; ++i) {
        const int h2 = 2 * h[n2];
        const int h3 = 3 * h[n3];
        const int h5 = 5 * h[n5];
        const int h7 = 7 * h[n7];
        h[i] = min(min(h2, h3), min(h5, h7));

        if (h[i] == h2) ++n2;
        if (h[i] == h3) ++n3;
        if (h[i] == h5) ++n5;
        if (h[i] == h7) ++n7;
    }

    while (cin >> n && n) {
        const int rem100 = n % 100;
        const int rem = n % 10;
        const string id = (rem100 != 11 && rem100 != 12 && rem100 != 13 && rem > 0 && rem < 4) ? index[rem - 1] : "th";
        cout << "The " << n << id << " humble number is " << h[n-1] << "." << endl;
    }

    return 0;
}