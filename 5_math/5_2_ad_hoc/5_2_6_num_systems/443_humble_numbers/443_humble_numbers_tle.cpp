#include <iostream>
#include <vector>
#include <cstdio>
using namespace std;

typedef long long ll;

int n;
const string index[] = {"st", "nd", "rd"};

void setVal(vector<char> & v, const int val, const size_t shift)
{
	for (int i = val; i < v.size(); i += val)
	{
		v[i] |= 1 << shift;
	}
}

int main()
{
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
	
	const int size = 2000000001;
	vector<char> v(size);
	
	setVal(v, 2, 0); 
	setVal(v, 3, 1);
	setVal(v, 5, 2);
	setVal(v, 7, 3);
	
	vector<int> h;
	for (int i = 0; i < size; ++i) {
		if (v[i] == 0xf) h.push_back(i);
	}
	
	while (cin >> n && n) {
		const int rem = n % 10;
		const string id = (rem < 4) ? index[rem - 1] : "th";
		cout << "The " << n << id << " humble number is " << h[n] "." << endl;		
	}
	
	
	return 0;
}