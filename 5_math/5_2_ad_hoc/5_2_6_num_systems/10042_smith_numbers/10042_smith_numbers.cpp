#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <vector>
using namespace std;

typedef long long ll;

const int SIZE = 1e9 + 4;
const int pSIZE = sqrt(SIZE) + 1;
int t, n;

ll sumDigits(int k)
{
    ll sumOfDigits = 0;
    while (k) {
        const int d = k % 10;
        sumOfDigits += d;
        k = k / 10;
    }
    return sumOfDigits;
}

bool checkPrime(const int v, const vector<int> & prime, const vector<bool> & isPrime)
{
    if (v < pSIZE) return isPrime[v];
    for (int i = 0; i < prime.size(); ++i) {
        if (v % prime[i] == 0) {
            return false;
        }
    }
    return true;
}

int smith(const int v, const vector<int> & prime, const vector<bool> & isPrime)
{
    int res = -1;
    for (int i = v + 1; i < SIZE && (res == -1); ++i) {
        if (checkPrime(i, prime, isPrime)) continue;

        int k = i;
        int cur = 0;
        ll sumOfFactors = 0;
        while (cur < prime.size() && k > 1) {
            while (k % prime[cur] == 0) {
                sumOfFactors += sumDigits(prime[cur]);
                k /= prime[cur];
            }
            ++cur;
        }
        if (k > 1) {
            sumOfFactors += sumDigits(k);
        }

        const ll sumOfDigits = sumDigits(i);
        if (sumOfDigits == sumOfFactors) {
            res = i;
        }
    }

    return res;
}

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    vector<bool> isPrime(pSIZE, true);
    isPrime[0] = false;
    isPrime[1] = false;

    for (int i = 2; i < pSIZE; ++i) {
        if (isPrime[i]) {
            for (int j = 2 * i; j < pSIZE; j += i) {
                isPrime[j] = false;
            }
        }
    }

    vector<int> p;
    for (int i = 2; i < pSIZE; ++i) {
        if (isPrime[i]) p.push_back(i);
    }

    cin >> t;
    while (t--) {
        cin >> n;
        cout << smith(n, p, isPrime) << endl;
    }

    return 0;
}