#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <sstream>
using namespace std;

typedef long long ll;

ll poly(const int x, const vector<int> & c)
{
   const int n = (int)c.size() - 1;
   if (n == 0) return c[0];

   ll res = c[n];
   ll xCur = 1ll;
   for (int i = 1; i < c.size(); ++i) {
      xCur *= x;
      res += c[n - i] * xCur;
   }

   return res;
}

int main()
{
   //freopen("in.txt", "r", stdin);
   //freopen("out.txt", "w", stdout);

   while (!cin.eof())
   {
      string line;
      getline(cin, line);
      if (line.empty()) {
         break;
      }

      vector<int> c;
      stringstream ss(line);
      int v = 0;
      while (ss >> v) {
         c.push_back(v);
      }

      getline(cin, line);

      stringstream ssX(line);
      vector<int> x;
      v = 0;
      while (ssX >> v) {
         x.push_back(v);
      }

      for (int i = 0; i < x.size(); ++i) {
         cout << poly(x[i], c);
         if (i != x.size() - 1) cout << " ";
      }
      cout << endl;
   }

   return 0;
}