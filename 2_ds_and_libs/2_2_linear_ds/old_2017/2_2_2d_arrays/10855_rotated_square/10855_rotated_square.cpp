#include <string>
#include <sstream>
#include <iostream>
#include <cstdio>
#include <vector>
using namespace std;

#define DEBUG 1

void rotate(const vector<string>& in, vector<string>& out)
{
   const int n = in.size();

   for (int i = 0; i < n; ++i) {
      string row;
      for (int j = 0; j < n; ++j) {
         row.push_back(in[j][n - 1 - i]);
      }
      out.push_back(row);
   }
}

int count(const vector<string>&b, const vector<string>& s)
{
   int res = 0;
   const int N = (int)b.size();
   const int n = (int)s.size();
   const int end = N - n + 1;
   for (int i = 0; i < end; ++i) {
      size_t pos = b[i].find(s[0]);
      while (pos != string::npos) {
         bool yes = true;
         for (int j = 1; j < n && yes && (i + j < N); ++j) {
            const string sub = b[i + j].substr(pos, n);
            yes = (s[j].compare(sub) == 0);
         }
         if (yes) ++res;
         pos = b[i].find(s[0], pos+1);
      }
   }

   return res;
}

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   int N = 0;
   int n = 0;
   while (scanf("%d %d\n", &N, &n), N || n) {
      vector<string> b;
      for (int i = 0; i < N; ++i) {
         string line;
         getline(cin, line);
         b.push_back(line);
      }

      vector<string> s0;
      for (int i = 0; i < n; ++i) {
         string line;
         getline(cin, line);
         s0.push_back(line);
      }

      vector<string> s270;
      rotate(s0, s270);

      vector<string> s180;
      rotate(s270, s180);

      vector<string> s90;
      rotate(s180, s90);

      cout << count(b, s0) << " ";
      cout << count(b, s90) << " ";
      cout << count(b, s180) << " ";
      cout << count(b, s270) << endl;
   }

   return 0;
}