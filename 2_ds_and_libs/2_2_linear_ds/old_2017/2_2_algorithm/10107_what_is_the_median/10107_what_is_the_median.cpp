#include <vector>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <cmath>
using namespace std;

typedef long long ll;

#define DEBUG 1

ll median(vector<int> & v)
{
   ll res = 0;

   const int n = (int)v.size();
   const int mid = n / 2;

   //sort(v.begin(), v.end());
   nth_element(v.begin(), v.begin() + mid, v.begin() + n);
   res = (ll)v[mid];

   if (n > 0 && n % 2 == 0) {
      const int prev = mid - 1;
      nth_element(v.begin(), v.begin() + prev, v.end());
      res = (ll)floor(0.5*(double)(res + v[prev]));
   }

   return res;
}

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   vector<int> v;
   string line;
   while (!cin.eof()) {
      int n = -1;
      cin >> n;
      if (n < 0) break;

      v.push_back(n);
      const ll res = median(v);
      cout << res << endl;
   }

   return 0;
}