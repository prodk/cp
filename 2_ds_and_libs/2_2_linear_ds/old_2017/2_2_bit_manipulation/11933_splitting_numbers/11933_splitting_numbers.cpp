#include <cstdio>
using namespace std;

#define DEBUG 1

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   int n = 0;
   while (scanf("%d\n", &n), n) {
      int a = 0;
      int b = 0;
      int j = 0;
      int cnt = 0;
      while (n != 0) {
         if (n & 1) {
            ++cnt;
            if (cnt % 2) a |= (1 << j);
            else b |= (1 << j);
         }
         n = n >> 1;
         ++j;
      }

      printf("%d %d\n", a, b);
   }

   return 0;
}