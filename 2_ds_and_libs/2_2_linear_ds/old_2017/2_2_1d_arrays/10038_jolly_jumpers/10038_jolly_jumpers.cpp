#include <vector>
#include <iostream>
#include <sstream>
#include <string>
#include <algorithm>
using namespace std;

#define DEBUG 1

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   //freopen("out.txt", "w", stdout);
#endif

   while (!cin.eof()) {
      string line;
      getline(cin, line);
      stringstream ss(line);
      int n = 0;
      ss >> n;

      vector<int> v(n);
      for (int i = 0; i < n; ++i) ss >> v[i];

      vector<bool> b(n, false);
      for (int i = 1; i < n; ++i) {
         const int diff = abs(v[i] - v[i - 1]);
         if (diff < n) b[diff] = true;
      }

      bool res = true;
      for (int i = 1; i < n && res; ++i) {
         res = b[i];
      }

      if (res) cout << "Jolly" << endl;
      else cout << "Not jolly" << endl;
   }

   return 0;
}