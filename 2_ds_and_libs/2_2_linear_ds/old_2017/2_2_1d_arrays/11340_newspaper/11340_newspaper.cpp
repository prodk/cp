#include <string>
#include <sstream>
#include <unordered_map>
#include <iomanip>
#include <iostream>
using namespace std;

#define DEBUG 1

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   int t = 0;
   string line;
   getline(cin, line);
   stringstream ss(line);

   ss >> t;
   ss.clear();
   while (t--) {
      getline(cin, line);
      stringstream lss(line);

      int k = 0;
      lss >> k;

      unordered_map<char, long long> m;
      while (k--) {
         getline(cin, line);
         stringstream nss(line);
         //ss << (line);
         char c = 0;
         nss >> c;
         nss >> m[c];
      }

      getline(cin, line);
      stringstream mss(line);

      int numOfLines = 0;
      mss >> numOfLines;

      long long sum = 0l;
      while (numOfLines--) {
         getline(cin, line);
         for (auto i : line) if (m.find(i) != m.end()) sum += m[i];
      }

      const double res = (double)sum / 100.0;
      cout << setprecision(2) << fixed << res << "$" << endl;

   }

   return 0;
}