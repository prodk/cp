#include <stack>
#include <algorithm>
#include <vector>
#include <iostream>
#include <cstdio>
#include <string>
using namespace std;

#define DEBUG 1

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   string in;
   string out;
   cin >> in;
   cin >> out;

   while (!cin.eof()) {
      cout << "[" << endl;
      const int n = (int)in.size();
      string res(2 * n, 'i');
      fill(res.begin() + n, end(res), 'o');
      do {
         if (res[0] == 'o') continue;
         stack<char> s;
         string w;
         int cur = 0;
         for (auto c : res) {
            if (c == 'i') {
               s.push(in[cur]);
               ++cur;
            }
            else if (s.empty()) break;
            else if (c == 'o') {
               w.push_back(s.top());
               s.pop();
            }
            if (!w.empty() && w[w.size() - 1] != out[w.size() - 1]) break;
         }

         if (w == out) {
            for (auto c : res) cout << c << " ";
            cout << endl;
         }
      } while (next_permutation(begin(res), end(res)));
      cout << "]" << endl;
      cin >> in;
      cin >> out;
   }

   return 0;
}