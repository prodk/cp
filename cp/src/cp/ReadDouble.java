package cp;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.Scanner;


public class ReadDouble {

	public static void main(String[] args) {
		//Scanner in = new Scanner(System.in);
		//double v = in.nextDouble();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		double v;
		try {
			v = Double.parseDouble(br.readLine());
			System.out.println(new DecimalFormat("###.###").format(v));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
