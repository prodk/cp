#include <iostream>
#include <cmath>
#include <sstream>
#include <iomanip>
using namespace std;

int main()
{
   auto n = 0;
   cin >> n;

   auto pi = 4.0*std::atan(1.0);
   stringstream ss;
   ss << fixed << setprecision(n) << pi;

   cout << ss.str() << endl;

   return 0;
}