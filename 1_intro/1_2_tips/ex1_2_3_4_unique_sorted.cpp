#include <iostream>
#include <set>
using namespace std;

int main()
{
   auto n = 0;
   cin >> n;
   
   std::set<int> s;
   auto i = 0;
   while (n--) {
      cin >> i;
	  s.insert(i);
   }
   
   for (auto c : s) cout << c << " ";
   cout << endl;
   
   return 0;
}