#include <iostream>
#include <string>
#include <regex>
using namespace std;

int main()
{
   const string s = "line: a70 and z72 will be replaced, aa24 and a872 will not.";
   regex e(" [[:alpha:]][[:digit:]][[:digit:]] ");
   cout << regex_replace(s, e, " *** ", regex_constants::format_default) << endl;

   return 0;
}