#include <algorithm>
#include <iostream>
#include <string>
using namespace std;

int main()
{
   auto n = 10;
   cin >> n;

   string s(n, ' ');
   for (auto i = 0; i < n; ++i) s[i] = 'a' + i;

   auto count = 0u;
   do {
      for (auto c : s) cout << c << " ";
      cout << endl;
      ++count;
   } while (next_permutation(begin(s), end(s)));

   cout << "total " << count << endl;

   return 0;
}