#include <iostream>
#include <vector>
using namespace std;

int main()
{
   int n = 0;
   cin >> n;
   
   vector<int> v(n);
   
   for (auto i = 0; i < n; ++i) v[i] = i;
   
   const auto k = 1 << n;
   for (auto i = 0; i < k; ++i) {
      for (auto j = 0; j < n; ++j) {
	     // select those numbers that belong to the current subset i, i.e. whose bits are set in i
	     if (i & 1 << j) {
		    cout << v[j] << " ";
		 }
	  }
	  cout << endl;
   }   
   
   return 0;
}