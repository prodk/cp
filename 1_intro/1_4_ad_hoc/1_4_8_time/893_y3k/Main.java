import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.Scanner;

class Main
{
   public static void main(String [] args)
   {
      Scanner in = new Scanner(System.in);
	  while (true)
	  {
	     int days = in.nextInt();
		 int d = in.nextInt();
		 int m = in.nextInt();
		 int y = in. nextInt();

		 if (days == 0 && d == 0 && m == 0 && y == 0) break;
		 
		 Calendar calendar = new GregorianCalendar(y, m - 1, d);
		 calendar.add(Calendar.DATE, days);
		 System.out.println(calendar.get(Calendar.DATE) + " " + (calendar.get(Calendar.MONTH) + 1) + " " + calendar.get(Calendar.YEAR));
	  }
   }
}