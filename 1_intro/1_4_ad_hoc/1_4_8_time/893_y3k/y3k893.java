import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;

import java.util.Scanner;

class y3k893
{
   public static void main(String [] args)
   {
      Scanner in = new Scanner(System.in);
	  while (true)
	  {
	     long days = in.nextLong();
		 long ms = days * 24 * 60 * 60 * 1000;
		 int dd = in.nextInt();
		 int mm = in.nextInt();
		 int y = in. nextInt();

		 if (ms == 0 && dd == 0 && mm == 0 && y == 0) break;		 
		 
		 String s = Integer.toString(dd) + " " + Integer.toString(mm) + " " + Integer.toString(y);
		 System.out.println(s);
		 System.out.println(ms);
		 
		 SimpleDateFormat df = new SimpleDateFormat("dd mm yyyy");
		 try
		 {
		    Date date = df.parse(s);
			long epoch = date.getTime();
			long finalEpoch = epoch + ms;
			System.out.println(df.format(new Date(finalEpoch)));
		 }
		 catch (ParseException e)
		 {		    
		 }
	  }
   }
}