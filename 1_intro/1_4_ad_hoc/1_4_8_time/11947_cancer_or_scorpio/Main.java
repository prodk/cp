import java.util.GregorianCalendar;
import java.util.Calendar;
import java.io.*;

class Main
{
   static public class Group
   {
      public int end;
	  public String less;
	  public String greater;
	  Group(int endIn, String lessIn, String greaterIn)
	  {
	     end = endIn;
		 less = lessIn;
		 greater = greaterIn;
	  }
   }
   
   public static void main(String [] args) throws IOException 
   {
      Group g[] = new Group[12];
	  g[0] = new Group(20, "capricorn", "aquarius");
	  g[1] = new Group(19, "aquarius", "pisces");
	  g[2] = new Group(20, "pisces", "aries");
	  g[3] = new Group(20, "aries", "taurus");
	  g[4] = new Group(21, "taurus", "gemini");
	  g[5] = new Group(21, "gemini", "cancer");
	  g[6] = new Group(22, "cancer", "leo");
	  g[7] = new Group(21, "leo", "virgo");
	  g[8] = new Group(23, "virgo", "libra");
	  g[9] = new Group(23, "libra", "scorpio");
	  g[10] = new Group(22, "scorpio", "sagittarius");
	  g[11] = new Group(22, "sagittarius", "capricorn");
   
	  BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	  String str = br.readLine();
	  int n = Integer.parseInt(str);
	  int count = 0;
	  int delta = 40 * 7;
	  while (count < n)
	  {
	     ++count;
	     str = br.readLine();
		 String tmp = str.substring(0, 2);
		 int m = Integer.parseInt(tmp);
		 tmp = str.substring(2, 4);
		 int d = Integer.parseInt(tmp);
		 tmp = str.substring(4);
		 int y = Integer.parseInt(tmp);
		 Calendar cal = new GregorianCalendar(y, m - 1, d);
		 
		 cal.add(Calendar.DATE, delta);
		 
		 int month = cal.get(Calendar.MONTH);
		 String res = (cal.get(Calendar.DATE) <= g[month].end) ? g[month].less : g[month].greater;

		 System.out.printf("%d %02d/%02d/%04d %s\n", count, (cal.get(Calendar.MONTH) + 1), cal.get(Calendar.DATE), cal.get(Calendar.YEAR), res);		 
	  }
   }
}