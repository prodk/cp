#include <iostream>
#include <cmath>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <set>
#include <map>
#include <queue>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <cstdio>
#include <ctime>
#include<stack>
#include<unordered_map>
#include<unordered_set>
#include<bitset>
using namespace std;

typedef vector<int> vi;
typedef vector<string> vs;
typedef pair<int, int> ii;
typedef vector<ii> vii;

#define DEBUG 0

int main()
{
#if DEBUG
   clock_t t = clock();
   freopen("in.txt", "r", stdin);
   freopen("out.txt","w",stdout);
#endif
   int m = 0;
   int n = 0;
   cin >> m;
   cin >> n;
   while (m != 0 || n != 0) {
      int res = ceil((double)m / 2.0)*ceil((double)n / 2) + (m / 2)*(n / 2);
      if (m == 0 || n == 0) res = 0;
      else if (n == 1) res = m;
      else if (m == 1) res = n;
      else if (m == 2) {
         const int k = n / 4;
         const int rem = n % 4;
         const int ad = 2*min(rem, 2);
         res = k * 4 + ad;
      }
      else if (n == 2) {
         const int k = m / 4;
         const int rem = m % 4;
         const int ad = 2*min(rem, 2);
         res = k * 4 + ad;
      }
      cout << res << " knights may be placed on a " << m << " row " << n << " column board." << endl;
      cin >> m;
      cin >> n;
   }

#if DEBUG
   t = clock() - t;
   //printf("%f s\n", ((float)t) / CLOCKS_PER_SEC);
   //cin.get(); cin.get();
#endif

   return 0;
}