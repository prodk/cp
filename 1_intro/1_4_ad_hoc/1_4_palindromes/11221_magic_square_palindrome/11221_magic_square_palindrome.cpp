#include <string>
#include <iostream>
#include <cstdio>
#include <cctype>
#include <cmath>
#include <sstream>

using namespace std;

#define DEBUG 1

bool isPalindrome(const string& s)
{
   bool res = true;

   int lo = 0;
   int hi = (int)s.size() - 1;

   while (lo < hi && res) {
      while (lo < hi && !isalpha(s[lo])) ++lo;
      while (lo < hi && !isalpha(s[hi])) --hi;

      if (s[lo] != s[hi]) res = false;
      ++lo;
      --hi;
   }

   return res;
}

int main()
{
#ifdef DEBUG 
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   int t = 0;
   string num;
   getline(cin, num);
   stringstream nline(num);
   nline >> t;

   int count = 0;
   while (t--) {
      cout << "Case #" << count << ":" << endl;
      ++count;
      string line;
      getline(cin, line);

      string s;
      for (auto c : line) {
         if (isalpha(c)) s.push_back(c);
      }

      const int len = (int)s.size();
      const int n = (int)sqrt((double)len);

      int res = 0;
      if (n*n == len && isPalindrome(s)) {
         string t;
         for (int x = 0; x < n; ++x) {
            for (int y = 0; x + y < len; y += n) {
               t.push_back(s[x + y]);
            }
         }
         if (isPalindrome(t)) res = n;
      }
      
      if (res <= 0) cout << "No magic :(";
      else cout << res;
      if (t > 0) cout << endl;
   }

   return 0;
}