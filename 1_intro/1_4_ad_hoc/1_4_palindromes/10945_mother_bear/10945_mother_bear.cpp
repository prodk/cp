#include <iostream>
#include <cmath>     // hypot, abs(float)
#include <vector>
#include <string>    // to_string, stoi, string::npos, getline(std::cin, line), find, rfind
#include <sstream>
#include <algorithm> // count, fill, unique, max_element, next_permutation, set_intersection, nth_element, sort
#include <set>
#include <map>
#include <queue>    // priority_queue
#include <fstream>
#include <iomanip>  // setprecision
#include <cstdio>   // printf, scanf
#include <ctime>
#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <bitset>
#include <climits>  // INT_MAX, INT_MIN, LONG_MIN, LLONG_MAX etc.
#include <locale>   // toupper, tolower
#include <cctype>   // isalnum, isdigit
#include <iterator> // distance
#include <utility>  // make_pair
#include <regex>
#include <numeric>  // accumulate
using namespace std;

typedef vector<int> vi;
typedef vector<string> vs;
typedef pair<int, int> ii;
typedef vector<ii> vii;

#define DEBUG 0     // set to 0 before submit

bool isPalindrome(string & s)
{
   bool result = true;

   //transform(s.begin(), s.end(), s.begin(), std::tolower);

   int lo = 0;
   int hi = (int)s.size() - 1;
   while (lo <= hi && result) {
      while (lo < hi && (s[lo] == ' ' || s[lo] == '.' || s[lo] == ',' || s[lo] == '!' || s[lo] == '?')) ++lo;
      while (lo < hi && (s[hi] == ' ' || s[hi] == '.' || s[hi] == ',' || s[hi] == '!' || s[hi] == '?')) --hi;
      if (tolower(s[lo]) != tolower(s[hi])) result = false;
      ++lo;
      --hi;
   }

   return result;
}

int main()
{
#if DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt","w",stdout);
#endif

   const string yes = "You won\'t be eaten!";
   const string no = "Uh oh..";
   string line;
   while (!getline(cin, line).eof()) {
      if (line.compare("DONE") == 0) break;
      if (isPalindrome(line)) cout << yes << endl;
      else cout << no << endl;
   }

   return 0;
}