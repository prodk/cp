#include <iostream>
#include <cstdio>   // printf, scanf, freopen
#include <vector>
#include <string>    // to_string, stoi, string::npos, getline(std::cin, line), find, rfind
#include <cmath>     // hypot, abs(float), floor, ceil
#include <sstream>
#include <algorithm> // count, fill, unique, max_element, next_permutation, set_intersection, nth_element, sort, lower_bound, binary_search
#include <numeric>  // accumulate
#include <set>
#include <map>
#include <unordered_map>
#include <functional> // greater<>
#include <queue>    // priority_queue
#include <iomanip>  // setprecision
#include <climits>  // INT_MAX, INT_MIN, LONG_MIN, LLONG_MAX etc.
#include <cstring>  // memset, memcpy
#include <locale>   // toupper, tolower
#include <cctype>   // isalnum, isdigit
using namespace std;

typedef vector<int> vi;
typedef vector<string> vs;
typedef pair<int, int> ii;
typedef vector<ii> vii;

bool isPalindrome(const string & s)
{
    auto res = true;

    const auto len = s.size();
    const auto half = len / 2;
    for (auto i = 0; i < half && res; ++i)
        res = s[i] == s[len - i - 1];

    return res;
}

unordered_map<char, char> table = { { 'A','A' },{ 'E','3' },{ 'H','H' },{ 'I','I' },{ 'J','L' },{ 'L','J' },
{ 'M','M' },{ 'O','O' },{ 'S','2' },{ 'T','T' },{ 'U','U' },{ 'V','V' },
{ 'W','W' },{ 'X','X' },{ 'Y','Y' },{ 'Z','5' },{ '1','1' },{ '2','S' },
{ '3','E' },{ '5','Z' },{ '8','8' }
};

bool isMirrored(const string & s)
{
    auto res = true;
    const auto len = s.size();
    const auto half = (0 == (len % 2)) ? len / 2 : len / 2 + 1;
    for (auto i = 0; i < half && res; ++i)
        res = (table.find(s[i]) != table.end()) && (table[s[i]] == s[len - i - 1]);

    return res;
}

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    while (!cin.eof()) {
        string s;
        cin >> s;

        if (s.empty()) break;
        if (s.size() == 1) {
            if (table.find(s[0]) != table.end() && table[s[0]] == s[0]) cout << s << " -- is a mirrored palindrome." << endl;
            else cout << s << " -- is a regular palindrome." << endl;
        }
        else {
            const auto p = isPalindrome(s);
            const auto m = isMirrored(s);

            if (!p && !m) cout << s << " -- is not a palindrome." << endl;
            if (p && !m)  cout << s << " -- is a regular palindrome." << endl;
            if (!p && m)  cout << s << " -- is a mirrored string." << endl;
            if (p && m)   cout << s << " -- is a mirrored palindrome." << endl;
        }
        cout << endl;
    }

    return 0;
}