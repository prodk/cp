#include <iostream>
#include <cstdio>   // printf, scanf, freopen
#include <vector>
#include <string>    // to_string, stoi, string::npos, getline(std::cin, line), find, rfind
#include <cmath>     // hypot, abs(float), floor, ceil
#include <sstream>
#include <algorithm> // count, fill, unique, max_element, next_permutation, set_intersection, nth_element, sort, lower_bound, binary_search
#include <numeric>  // accumulate
#include <set>
#include <map>
#include <unordered_map>
#include <functional> // greater<>
#include <queue>    // priority_queue
#include <iomanip>  // setprecision
#include <climits>  // INT_MAX, INT_MIN, LONG_MIN, LLONG_MAX etc.
#include <cstring>  // memset, memcpy
#include <locale>   // toupper, tolower
#include <cctype>   // isalnum, isdigit
using namespace std;

typedef vector<int> vi;
typedef vector<string> vs;
typedef pair<int, int> ii;
typedef vector<ii> vii;

bool isPalindrome(const string & line)
{
    auto res = true;

    auto s = 0;
    auto e = (int)line.size() - 1;
    while (s < e && res) {
        while (!isalpha(line[s]) && s < e) ++s;
        while (!isalpha(line[e]) && s < e) --e;
        res = toupper(line[s]) == toupper(line[e]);
        ++s;
        --e;
    }

    return res;
}

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    string line;
    while (getline(cin, line))
    {
        if (line == "DONE") break;
        if (isPalindrome(line)) cout << "You won't be eaten!" << endl;
        else cout << "Uh oh.." << endl;
    }
    
    return 0;
}