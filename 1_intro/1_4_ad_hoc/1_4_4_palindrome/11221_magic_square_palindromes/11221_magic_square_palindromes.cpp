#include <iostream>
#include <cstdio>   // printf, scanf, freopen
#include <vector>
#include <string>    // to_string, stoi, string::npos, getline(std::cin, line), find, rfind
#include <cmath>     // hypot, abs(float), floor, ceil
#include <sstream>
#include <algorithm> // count, fill, unique, max_element, next_permutation, set_intersection, nth_element, sort, lower_bound, binary_search
#include <numeric>  // accumulate
#include <set>
#include <map>
#include <unordered_map>
#include <functional> // greater<>
#include <queue>    // priority_queue
#include <iomanip>  // setprecision
#include <climits>  // INT_MAX, INT_MIN, LONG_MIN, LLONG_MAX etc.
#include <cstring>  // memset, memcpy
#include <locale>   // toupper, tolower
#include <cctype>   // isalnum, isdigit
using namespace std;

typedef vector<int> vi;
typedef vector<string> vs;
typedef pair<int, int> ii;
typedef vector<ii> vii;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    auto t = 0;
    cin >> t;
    cin.ignore();

    for (auto count = 1; count <= t; ++count) {
        string line;
        getline(cin, line);

        string row;
        for (auto i = 0; i < line.size(); ++i) {
            const auto c = line[i];
            if (isalpha(c)) row.push_back(c);
        }

        const auto len = row.size();
        const int k = static_cast<int>(sqrt(static_cast<double>(len)));
        auto result = len == k * k;
        for (auto r = 0; r < k && result; ++r)
            for (auto c = 0; c < k && result; ++c) {
                result = (row[r*k + c] == row[(k - r - 1)*k + k - c - 1])
                    && (row[c*k + r] == row[(k - c - 1)*k + k - r - 1]);
            }

        cout << "Case #" << count << ":" << endl;
        if (result) cout << k << endl;
        else cout << "No magic :(" << endl;
    }

    return 0;
}