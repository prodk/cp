#include <iostream>
#include <cstdio>   // printf, scanf, freopen
#include <vector>
#include <string>    // to_string, stoi, string::npos, getline(std::cin, line), find, rfind
#include <cmath>     // hypot, abs(float), floor, ceil
#include <sstream>
#include <algorithm> // count, fill, unique, max_element, next_permutation, set_intersection, nth_element, sort, lower_bound, binary_search
#include <numeric>  // accumulate
#include <set>
#include <map>
#include <unordered_map>
#include <functional> // greater<>
#include <queue>    // priority_queue
#include <iomanip>  // setprecision
#include <climits>  // INT_MAX, INT_MIN, LONG_MIN, LLONG_MAX etc.
#include <cstring>  // memset, memcpy
#include <locale>   // toupper, tolower
#include <cctype>   // isalnum, isdigit
using namespace std;

typedef vector<int> vi;
typedef vector<string> vs;
typedef pair<int, int> ii;
typedef vector<ii> vii;

bool isPalindrome(const string & line)
{
    auto res = true;
    auto s = 0;
    auto e = static_cast<int>(line.size()) - 1;
    while (s < e && res) {
        while (!isalpha(line[s]) && s < e) ++s;
        while (!isalpha(line[e]) && e > s) --e;
        res = line[s] == line[e];
        ++s;
        ++e;
    }

    return res;
}

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    auto t = 0;
    cin >> t;
    string line;
    getline(cin, line);
    auto count = 0;
    while (t--) {
        ++count;
        getline(cin, line);

        string row;
        for (auto i = 0; i < line.size(); ++i) {
            const auto c = line[i];
            if (isalpha(c)) row.push_back(c);
        }

        const auto len = row.size();
        const int sqrtLen = static_cast<int>(sqrt(static_cast<double>(len)));
        auto result = len == sqrtLen * sqrtLen;
        result = result && isPalindrome(row);
        if (result) {
            string columns;
            for (auto c = 0; c < sqrtLen; ++c)
                for (auto r = 0; r < sqrtLen; ++r) {
                    columns.push_back(row[r*sqrtLen + c]);
                }

            result = isPalindrome(columns);
        }

        cout << "Case #" << count << ":" << endl;
        if (result) cout << sqrtLen << endl;
        else cout << "No magic :(" << endl;
    }

    return 0;
}