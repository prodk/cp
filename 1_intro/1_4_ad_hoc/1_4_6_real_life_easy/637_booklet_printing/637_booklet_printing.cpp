#include <iostream>
#include <cstdio>   // printf, scanf, freopen
#include <vector>
#include <string>    // to_string, stoi, string::npos, getline(std::cin, line), find, rfind
#include <cmath>     // hypot, abs(float), floor, ceil
#include <sstream>
#include <algorithm> // count, fill, unique, max_element, next_permutation, set_intersection, nth_element, sort, lower_bound, binary_search, transform
#include <numeric>  // accumulate
#include <set>
#include <map>
#include <unordered_map>
#include <functional> // greater<>
#include <queue>    // priority_queue
#include <iomanip>  // setprecision
#include <climits>  // INT_MAX, INT_MIN, LONG_MIN, LLONG_MAX etc.
#include <cstring>  // memset, memcpy
#include <locale>   // toupper, tolower
#include <cctype>   // isalnum, isdigit
using namespace std;

typedef vector<int> vi;
typedef vector<string> vs;
typedef pair<int, int> ii;
typedef vector<ii> vii;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    auto n = -1;
    while (cin >> n, n > 0) {
        const auto sheets = (int)ceil((double)n / 4.0);
        const auto sides = sheets * 2;

        vector<ii> res(sides);
        const auto last = sides - 1;
        for (auto i = 0; i < sheets; ++i) {
            const auto back = last - i * 2;
            if (back >= 0) {
                const auto left = sides - i * 2;
                res[back].first = (left > 0 && left <= n) ? left : 0;

                const auto right = sides + 1 + i * 2;
                res[back].second = (right <= n) ? right : 0;
            }

            const auto front = back - 1;
            if (front >= 0) {
                const auto left = sides + (i + 1) * 2;
                res[front].first = (left <= n) ? left : 0;

                const auto right = sides + 1 - (i + 1) * 2;
                res[front].second = (right > 0 && right <= n) ? right : 0;
            }
        }

        cout << "Printing order for " << n << " pages:" << endl;
        for (auto i = 0; i < sheets; ++i) {
            const ii & front = res[2 * i];
            if (front.first != 0 || front.second != 0) {
                const string frontLeft = (front.first == 0) ? "Blank" : to_string(front.first);
                const string frontRight = (front.second == 0) ? "Blank" : to_string(front.second);
                cout << "Sheet " << i + 1 << ", front: " << frontLeft << ", " << frontRight << endl;
            }

            const ii & back = res[2 * i + 1];
            if (back.first != 0 || back.second != 0) {
                const string backLeft = (back.first == 0) ? "Blank" : to_string(back.first);
                const string backRight = (back.second == 0) ? "Blank" : to_string(back.second);
                cout << "Sheet " << i + 1 << ", back : " << backLeft << ", " << backRight << endl;
            }
        }
    }

    return 0;
}