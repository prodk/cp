#include <cmath>
#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    auto n = 0;
    cin >> n;

    for (auto i = 0; i < n; ++i)
    {
        auto k = 0ll;
        cin >> k;
        auto m = 0ll;
        cin >> m;
        if (k < m || k < 0ll || m < 0ll) cout << "impossible" << endl;
        else {
            const auto a = (k + m) / 2ll;
            const auto b = (k - m) / 2ll;

            if (k == a + b && m == abs(a - b))
                cout << max<>(a, b) << " " << min<>(a, b) << endl;
            else
                cout << "impossible" << endl;
        }
    }

    return 0;
}