#include <iostream>
#include <cstdio>   // printf, scanf, freopen
#include <vector>
#include <string>    // to_string, stoi, string::npos, getline(std::cin, line), find, rfind
#include <cmath>     // hypot, abs(float), floor, ceil
#include <sstream>
#include <algorithm> // count, fill, unique, max_element, next_permutation, set_intersection, nth_element, sort, lower_bound, binary_search
#include <numeric>  // accumulate
#include <set>
#include <map>
#include <unordered_map>
#include <functional> // greater<>
#include <queue>    // priority_queue
#include <iomanip>  // setprecision
#include <climits>  // INT_MAX, INT_MIN, LONG_MIN, LLONG_MAX etc.
#include <cstring>  // memset, memcpy
#include <locale>   // toupper, tolower
#include <cctype>   // isalnum, isdigit
using namespace std;

typedef vector<int> vi;
typedef vector<string> vs;
typedef pair<int, int> ii;
typedef vector<ii> vii;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    string s;
    cin >> s;

    vs words;
    while (s != "#") {
        words.push_back(s);
        cin >> s;
    }

    map<string, ii> hist;
    for (auto i = 0; i < words.size(); ++i) {
        string w = words[i];
        transform(begin(w), end(w), begin(w), ::tolower);
        sort(begin(w), end(w));
        const auto e = hist.end();
        if (hist.find(w) == e) hist[w] = ii(1, i);
        else {
            ++hist[w].first;
            hist[w].second = i;
        }
    }

    set<string> res;
    for (auto iter : hist) {
        if (iter.second.first == 1) {
            res.insert(words[iter.second.second]);
        }
    }

    for (auto w : res) cout << w << endl;

    return 0;
}