#include <iostream>
#include <cstdio>   // printf, scanf, freopen
#include <vector>
#include <string>    // to_string, stoi, string::npos, getline(std::cin, line), find, rfind
#include <cmath>     // hypot, abs(float), floor, ceil
#include <sstream>
#include <algorithm> // count, fill, unique, max_element, next_permutation, set_intersection, nth_element, sort, lower_bound, binary_search
#include <numeric>  // accumulate
#include <set>
#include <map>
#include <functional> // greater<>
#include <queue>    // priority_queue
#include <iomanip>  // setprecision
#include <climits>  // INT_MAX, INT_MIN, LONG_MIN, LLONG_MAX etc.
#include <cstring>  // memset, memcpy
#include <locale>   // toupper, tolower
#include <cctype>   // isalnum, isdigit
using namespace std;

typedef vector<int> vi;
typedef vector<string> vs;
typedef pair<int, int> ii;
typedef vector<ii> vii;
using ic = pair<int, char>;
using cc = pair<char, char>;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    auto t = 0;
    cin >> t;
    auto count = 0;
    string s;
    getline(cin, s);
    while (t--) {
        ++count;
        getline(cin, s);
        vector<cc> cards;
        for (auto i = 0; i < s.size(); i += 3) {
            cards.emplace_back(make_pair(s[i], s[i + 1]));
        }

        vector<cc> topPile(begin(cards) + 27, end(cards));

        vector<cc> bottomPile(begin(cards), end(cards) - 24);

        auto y = 0;
        for (int i = 0; i < 3; ++i) {
            const char c = bottomPile.back().first;
            const auto x = isdigit(c) ? c - '0' : 10;
            y += x;
            bottomPile.pop_back();
            const auto numToDel = 10 - x;
            for (int j = 0; j < numToDel; ++j) bottomPile.pop_back();
        }

        for (int i = 0; i < 25; ++i) bottomPile.push_back(topPile[i]);

        cout << "Case " << count << ": " << bottomPile[y].first << bottomPile[y].second << endl;
    }

    return 0;
}