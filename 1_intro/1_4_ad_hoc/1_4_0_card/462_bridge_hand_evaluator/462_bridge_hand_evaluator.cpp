#include <iostream>
#include <cstdio>   // printf, scanf, freopen
#include <vector>
#include <string>    // to_string, stoi, string::npos, getline(std::cin, line), find, rfind
#include <cmath>     // hypot, abs(float), floor, ceil
#include <sstream>
#include <algorithm> // count, fill, unique, max_element, next_permutation, set_intersection, nth_element, sort, lower_bound, binary_search
#include <numeric>  // accumulate
#include <set>
#include <map>
#include <functional> // greater<>
#include <queue>    // priority_queue
#include <iomanip>  // setprecision
#include <climits>  // INT_MAX, INT_MIN, LONG_MIN, LLONG_MAX etc.
#include <cstring>  // memset, memcpy
#include <locale>   // toupper, tolower
#include <cctype>   // isalnum, isdigit
using namespace std;

typedef vector<int> vi;
typedef vector<string> vs;
typedef pair<int, int> ii;
typedef vector<ii> vii;

map<char, vi> suits;
vector<string> hand;
char maxSuit = 'S';

int countStoppedAndCalcInitialPoints(int& points)
{
   auto numOfStopped = 0;
   points = 0;
   maxSuit = 'S';
   for (auto m : suits) {
      const vi & v = m.second;
      const auto n = v.size();
      if ((suits[maxSuit].size() < n)
         || (suits[maxSuit].size() == n && maxSuit < m.first)) {
         maxSuit = m.first;
      }

      bool isStopped = false;
      for (auto i : v) {
         switch (hand[i][0]) {
         case 'A':
            points += 4;
            if (!isStopped) {
               isStopped = true;
               ++numOfStopped;
            }
            break;
         case 'K':
            points += 3;
            if (!isStopped && n > 1) {
               isStopped = true;
               ++numOfStopped;
            }

            if (n <= 1) points -= 1;
            break;
         case 'Q':
            points += 2;
            if (!isStopped && n > 2) {
               isStopped = true;
               ++numOfStopped;
            }
            if (n <= 2) points -= 1;
            break;
         case 'J':
            points += 1;
            if (n <= 3) points -= 1;
            break;

         default: continue;
         }
      }
   }

   return numOfStopped;
}

int main()
{
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);

   string line(1, ' ');
   while (!line.empty()) {
      getline(cin, line);
      if (line.empty()) break;
      suits.clear();
      hand.clear();

      for (auto i = 0u; i < line.size(); i += 3u) {
         string p(2, ' ');
         p[0] = line[i];
         p[1] = line[i + 1];
         hand.push_back(p);
         const int pId = (int)hand.size() - 1;

         if (suits.end() == suits.find(p[1]))
         {
            suits.insert(make_pair(p[1], vi(1, pId)));
         }
         else {
            suits[p[1]].push_back(pId);
         }
      }

      auto points = 0;
      auto numOfStopped = countStoppedAndCalcInitialPoints(points);
      if (points >= 16 && (4 == numOfStopped)) {
         cout << "BID NO-TRUMP" << endl;
      }
      else {
         for (auto m : suits) {
            const vi & v = m.second;
            const int n = (int)v.size();
            if (n == 2) ++points;
            else if ((n == 1) || (n == 0)) points += 2;
         }
         if (points < 14) cout << "PASS" << endl;
         else {
            cout << "BID " << maxSuit << endl;
         }
      }
   }
   
   return 0;
}