#include <iostream>
#include <cstdio>   // printf, scanf, freopen
#include <vector>
#include <string>    // to_string, stoi, string::npos, getline(std::cin, line), find, rfind
#include <cmath>     // hypot, abs(float), floor, ceil
#include <sstream>
#include <algorithm> // count, fill, unique, max_element, next_permutation, set_intersection, nth_element, sort, lower_bound, binary_search
#include <numeric>  // accumulate
#include <set>
#include <map>
#include <functional> // greater<>
#include <queue>    // priority_queue
#include <iomanip>  // setprecision
#include <climits>  // INT_MAX, INT_MIN, LONG_MIN, LLONG_MAX etc.
#include <cstring>  // memset, memcpy
#include <locale>   // toupper, tolower
#include <cctype>   // isalnum, isdigit
using namespace std;

typedef vector<int> vi;
typedef vector<string> vs;
typedef pair<int, int> ii;
typedef vector<ii> vii;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    int m = 0, n = 0;
    while (cin >> m >> n, (m || n)) {
        int res = 0;
        if (m >= 3 && n >= 3) {
            const auto kEven = ceil((float)m / 2.0f);
            const auto lEven = ceil((float)n / 2.0f);
            const auto kOdd = floor((float)m / 2.0f);
            const auto lOdd = floor((float)n / 2.0f);
            res = kEven * lEven + kOdd * lOdd;
        }
        else if (m > 0 && n > 0) {
            const auto a = max<>(m, n);
            const auto b = min<>(m, n);
            if (b == 1) res = a;
            else {
                const auto k = a / 4;
                auto rem = a % 4;
                rem = min<>(2, rem);
                res = b * (k * 2 + rem);
            }
        }
        cout << res << " knights may be placed on a " << m << " row " << n << " column board." << endl;
    }

    return 0;
}