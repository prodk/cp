#include <iostream>
#include <cmath>     // hypot, abs(float)
#include <vector>
#include <string>    // to_string, stoi, string::npos, getline, find, rfind
#include <sstream>
#include <algorithm> // count, fill, unique, max_element, next_permutation, set_intersection, nth_element, sort
#include <set>
#include <map>
#include <queue>    // priority_queue
#include <fstream>
#include <iomanip>  // setprecision
#include <cstdio>   // printf, scanf
#include <ctime>
#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <bitset>
#include <climits>  // INT_MAX, INT_MIN, LONG_MIN, LLONG_MAX etc.
#include <locale>   // toupper, tolower
#include <cctype>   // isalnum, isdigit
#include <iterator> // distance
#include <utility>  // make_pair
#include <regex>
#include <numeric>  // accumulate
using namespace std;

typedef vector<int> vi;
typedef vector<string> vs;
typedef pair<int, int> ii;
typedef vector<ii> vii;

#define DEBUG 1 // set to 0 before submit

bool isValid(const int id, const int n) {
   return (id >= 0) && (id < n);
}

bool replace(const char cur, const char neighb, map<char, char>& m) {
   return (neighb == m[cur]);
}

int main()
{
#if DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt","w",stdout);
#endif

   map<char, char> m;
   m.insert(make_pair('R', 'P'));
   m.insert(make_pair('S', 'R'));
   m.insert(make_pair('P', 'S'));

   int t = 0;
   cin >> t;
   while (t-- > 0) {
      int r = 0;
      //cin >> r;
      int c = 0;
      //cin >> c;
      int n = 0;
      //cin >> n;
      scanf("%d %d %d\n", &r, &c, &n);

      vs s;
      for (int i = 0; i < r; ++i) {
         string line;
         getline(std::cin, line);
         s.push_back(line);
      }

      for (int k = 0; k < n; ++k) {
         vs sNew;
         for (int i = 0; i < r; ++i) {
            const int top = i - 1;
            const int bot = i + 1;
            string newRow;
            for (int j = 0; j < c; ++j) {
               const int left = j - 1;
               const int right = j + 1;

               if (isValid(top, r) && replace(s[i][j], s[top][j], m)) {
                  newRow.push_back(s[top][j]);
                  continue;
               }
               if (isValid(bot, r) && replace(s[i][j], s[bot][j], m)) {
                  newRow.push_back(s[bot][j]);
                  continue;
               }
               if (isValid(left, c) && replace(s[i][j], s[i][left], m)) {
                  newRow.push_back(s[i][left]);
                  continue;
               }
               if (isValid(right, c) && replace(s[i][j], s[i][right], m)) {
                  newRow.push_back(s[i][right]);
                  continue;
               }

               newRow.push_back(s[i][j]);
            }
            sNew.push_back(newRow);
         }
         s.swap(sNew);
      }

      for (int i = 0; i < r; ++i) {
         cout << s[i] << endl;
         //if (i != r - 1) cout << endl;
      }

      if (t != 0) cout << endl;
   }

   return 0;
}