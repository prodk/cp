#include <iostream>
#include <cmath>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <set>
#include <map>
#include <queue>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <cstdio>
#include <ctime>
#include<stack>
#include<unordered_map>
#include<unordered_set>
#include<bitset>
using namespace std;

typedef vector<int> vi;
typedef vector<string> vs;
typedef pair<int, int> ii;
typedef vector<ii> vii;

#define DEBUG 1

int main()
{
#if DEBUG
   clock_t t = clock();
   freopen("in.txt", "r", stdin);
   //freopen("out.txt","w",stdout);
#endif
   //ios_base::sync_with_stdio(0);
   //cin.tie(0);

   int n = -1;
   while (scanf("%d", &n), n != -1) {
      string ans;
      cin >> ans;
      string guess;
      cin >> guess;

      int ah[26] = {0};
	  int size = 0;
      for (int i = 0; i < (int)ans.size(); ++i) {
         if (ah[ans[i] - 'a'] == 0) {
		    ah[ans[i] - 'a'] = 1;
			++size;
		 }
      }

      int count = 0;
      int i = 0;
      while (i < (int)guess.size() && count < 7 && size > 0) {
         if (ah[guess[i] - 'a'] == 0) ++count;
         else { 
		    ah[guess[i] - 'a'] = 0;
			--size;
		 }
         ++i;
      }

      cout << "Round " << n << endl;
      if (size == 0 && count < 7) cout << "You win." << endl;
      else if (size != 0 && count < 7) cout << "You chickened out." << endl;
      else if (size != 0 && count >= 7) cout << "You lose." << endl;
   }

#if DEBUG
   t = clock() - t;
   printf("%f s\n", ((float)t) / CLOCKS_PER_SEC);
   //cin.get(); cin.get();
#endif

   return 0;
}