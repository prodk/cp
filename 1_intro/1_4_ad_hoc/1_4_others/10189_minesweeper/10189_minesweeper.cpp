#include <iostream>
#include <cmath>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <set>
#include <map>
#include <queue>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <cstdio>
#include <ctime>
#include<stack>
#include<unordered_map>
#include<unordered_set>
#include<bitset>
using namespace std;

typedef vector<int> vi;
typedef vector<string> vs;
typedef pair<int, int> ii;
typedef vector<ii> vii;

#define DEBUG 0

int main()
{
#if DEBUG
   clock_t t = clock();
   freopen("in.txt", "r", stdin);
   freopen("out.txt","w",stdout);
#endif
   int field = 0;
   while (!cin.eof()) {
      ++field;

      int n = 0;
      cin >> n;
      int m = 0;
      cin >> m;
      if (n == 0 && m == 0) break;

      string line;
      vs s;
      for (int i = 0; i < n; ++i) {
         cin >> line;
         s.push_back(line);
      }

      vs res;
      for (int i = 0; i < n; ++i) {
         string row;
         for (int j = 0; j < m; ++j) {
            if (s[i][j] == '*') {
               row.push_back(s[i][j]);
               continue;
            }
            int count = 0;
            for (int x = -1; x <= 1; ++x) {
               for (int y = -1; y <= 1; ++y) {
                  if (x == 0 && y == 0) continue;

                  const int ny = i + y;
                  const int nx = j + x;
                  if (ny >= 0 && ny < n && nx >= 0 && nx < m && s[ny][nx] == '*') ++count;
               }
            }
            row.push_back('0' + count);
         }
         res.push_back(row);
      }

      if (field != 1) cout << "\n";
      cout << "Field #" << field << ":" << endl;
      for (int i = 0; i < (int)res.size(); ++i) {
         cout << res[i] << endl;
      }
      //cout << endl;
   }

#if DEBUG
   t = clock() - t;
   //printf("%f s\n", ((float)t) / CLOCKS_PER_SEC);
   //cin.get(); cin.get();
#endif

   return 0;
}