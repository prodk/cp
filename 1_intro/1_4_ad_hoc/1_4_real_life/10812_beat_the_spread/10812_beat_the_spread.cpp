#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cmath>
using namespace std;

#define DEBUG 1

typedef long long ll;

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   int n = 0;
   scanf("%d\n", &n);
   while (n--) {
      ll s = 0, d = 0;
      scanf("%lld %lld\n", &s, &d);

      if (s < d) {
         printf("impossible\n");
            continue;
      }
      const ll s10 = (d + s) / 2;
      const ll s11 = (s - d) / 2;
      const ll s00 = s - s10;
      const ll s01 = s - s11;

      const ll s1 = max<>(s10, s11);
      const ll s0 = min<>(s00, s01);
      if (s1 < 0 || s0 < 0 || (s1+s0 != s) || (abs(s1-s0)) != d) {
         printf("impossible\n");
         continue;
      }
      else printf("%lld %lld\n", s1, s0);
   }

   return 0;
}