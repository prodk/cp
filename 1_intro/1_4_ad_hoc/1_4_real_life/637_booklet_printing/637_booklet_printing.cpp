#include <vector>
#include <iostream>
#include <cstdio>
#include <map>
using namespace std;

#define DEBUG 1

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   int n = 0;
   cin >> n;

   while (n != 0) {
      cout << "Printing order for " << n << " pages:" << endl;
      int k = n;
      while (k % 4 != 0) ++k;

      vector<int> v(k);
      for (int i = 0; i < n; ++i) v[i] = i + 1;

      vector<pair<int, int> > f;
      vector<pair<int, int> > b;

      int lo = 0;
      int hi = k - 1;
      while (lo < hi) {
         if (v[hi] != 0 || v[lo] != 0) f.push_back(make_pair(v[hi], v[lo]));
         if (v[lo + 1] != 0 || v[hi - 1] != 0) b.push_back(make_pair(v[lo + 1], v[hi - 1]));

         lo += 2;
         hi -= 2;
      }

      int count = 0;
      while (count < f.size() || count < b.size()) {
         if (count < f.size()) {
            cout << "Sheet " << count + 1 << ", front: ";
            if (f[count].first == 0) cout << "Blank";
            else cout << f[count].first;
            if (f[count].second == 0) cout << ", Blank";
            else cout << ", " << f[count].second;
            cout << endl;
         }

         if (count < b.size()) {
            cout << "Sheet " << count + 1 << ", back : ";
            if (b[count].first == 0) cout << "Blank";
            else cout << b[count].first;
            if (b[count].second == 0) cout << ", Blank";
            else cout << ", " << b[count].second;
            cout << endl;
         }
         ++count;
      }

      cin >> n;
   }

   return 0;
}