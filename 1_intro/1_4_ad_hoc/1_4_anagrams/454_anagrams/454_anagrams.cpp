#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <algorithm>
#include <cstdio>
#include <sstream>
#include <cctype>
using namespace std;

#define DEBUG 1

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   int t = 0;
   string line;
   getline(cin, line);
   stringstream ss(line);
   ss >> t;

   getline(cin, line);

   while (t > 0) {
      map<string, pair<string, int>> m;
      while (1) {
         getline(cin, line);
         string sorted = line;
         if (line.empty()) {
            --t; break;
         }
         sorted.erase(remove_if(sorted.begin(), sorted.end(), [](char c) {
            return std::isspace(static_cast<unsigned char>(c));
         }), sorted.end());
         sort(sorted.begin(), sorted.end());

         m[line] = pair<string, int>(sorted, ++m[line].second);
      }

      map<string, vector<string>> res;
      for (auto it : m) {
         if (res.find(it.second.first) == res.end()) {
            vector<string> vs;
            res[it.second.first] = vs;
            for (int i = 0; i < it.second.second; ++i)
               res[it.second.first].push_back(it.first);
         }
         else {
            for (int i = 0; i < it.second.second; ++i)
               res[it.second.first].push_back(it.first);
         }
      }

      vector<pair<string, string>> sortedRes;
      for (auto it : res) {
         for (int i = 0; i < it.second.size() - 1; ++i) {
            for (int j = i + 1; j < it.second.size(); ++j) {
               sortedRes.push_back(make_pair(it.second[i], it.second[j]));
            }
         }
      }

      sort(begin(sortedRes), end(sortedRes));

      for (auto & p : sortedRes) cout << p.first << " = " << p.second << endl;

      if (t == 0) break;
      cout << endl;
   }

   return 0;
}