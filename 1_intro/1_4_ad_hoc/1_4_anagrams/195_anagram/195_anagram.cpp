#include <algorithm>
#include <vector>
#include <string>
#include <cstdio>
#include <sstream>
#include <iostream>
using namespace std;

#define DEBUG 1

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   int t = 0;
   string line;
   getline(cin, line);
   stringstream ss(line);
   ss >> t;

   while (t--) {
      string word;
      cin >> word;

      sort(word.begin(), word.end());

      vector<string> vs;
      do {
         vs.push_back(word);
      } while (next_permutation(word.begin(), word.end()));

      sort(begin(vs), end(vs), [](const string & lhs, const string & rhs)->bool {
         const int len = (int)lhs.size();
         for (int i = 0; i < len; ++i) {
            if (lhs[i] == rhs[i]) continue;
            const char l = tolower(lhs[i]);
            const char r = tolower(rhs[i]);
            if (l != r) {
               return l < r;
            }
            else {
               return lhs[i] < rhs[i];
            }
         }
      });

      for (auto & s : vs) cout << s << endl;
   }

   return 0;
}