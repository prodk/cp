#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <cstdio>
#include <sstream>
#include <cctype>
#include <algorithm>
#include <set>

using namespace std;

#define DEBUG 1

int main()
{
#ifdef DEBUG
   freopen("in.txt", "r", stdin);
   freopen("out.txt", "w", stdout);
#endif

   string line;
   vector<string> vs;
   while (!getline(cin, line).eof()) {
      if (line == "#") break;
      stringstream ss(line);

      string item;
      while (getline(ss, item, ' ')) vs.push_back(item);
   }

   vector<string> vl;
   for (auto & s : vs) {
      string item;
      for (auto c : s) item.push_back(tolower(c));
      vl.push_back(item);
   }

   for (auto & s : vl) sort(s.begin(), s.end());

   map<string, pair<int, int>> hist;

   for (int i = 0; i < (int)vl.size(); ++i) {
      if (hist.find(vl[i]) == hist.end()) hist[vl[i]] = make_pair(1, i);
      else ++hist[vl[i]].first;
   }

   set<string> res;
   for (auto it : hist) {
      if (it.second.first == 1) res.insert(vs[it.second.second]);
   }

   for (auto & s : vs) if (s.size() == 1) res.insert(s);

   for (auto & it : res) cout << it << endl;

   return 0;
}