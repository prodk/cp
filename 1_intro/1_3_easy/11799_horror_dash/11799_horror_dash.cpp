//accepted
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <cstdio>
using namespace std;

typedef vector<int> vi;
typedef vector<string> vs;
typedef pair<int, int> ii;
typedef vector<ii> vii;

int main()
{
   //freopen("11799.txt", "r", stdin);

   int t = 0;
   cin >> t;
   int cnt = 1;
   while (t-- > 0) {
      int n = 0;
      cin >> n;
      vi v(n);
      for (int i = 0; i < n; ++i) cin >> v[i];

      int m = v[0];
      for (auto i : v) m = max(m, i);

      cout << "Case " << cnt << ": " << m << endl;
	  ++cnt;
   }

   return 0;
}