#include <iostream>
#include <cmath>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <set>
#include <map>
#include <queue>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <cstdio>
#include <ctime>
#include<stack>
#include<unordered_map>
#include<unordered_set>
#include<bitset>
using namespace std;

typedef vector<int> vi;
typedef vector<string> vs;
typedef pair<int, int> ii;
typedef vector<ii> vii;

#define DEBUG 1

int main()
{
#if DEBUG
   clock_t t = clock();
   freopen("in.txt", "r", stdin);
   //freopen("out.txt","w",stdout);   
#endif
   //ios_base::sync_with_stdio(0);
   //cin.tie(0);

#if DEBUG
   t = clock() - t;
   printf("%f s\n", ((float)t) / CLOCKS_PER_SEC);
#endif

   return 0;
}