// accepted
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

typedef vector<int> vi;

int main()
{
   //freopen("11727.txt", "r", stdin);
   int t = 0;
   cin >> t;

   int count = 1;
   while (t-- > 0) {
      vi v(3);
      for (int i = 0; i < 3; ++i) cin >> v[i];

      sort(v.begin(), v.end());

      cout << "Case " << count << ": " << v[1] << endl;
      ++count;
   }
   return 0;
}