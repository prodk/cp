// accepted
#include <iostream>

using namespace std;

int main()
{
   int t = 0;
   cin >> t;

   while (t-- > 0) {
      int a = 0;
      cin >> a;
      int b = 0;
      cin >> b;
      if (a < b) cout << '<' << endl;
      else if (a > b) cout << '>' << endl;
      else if (a == b) cout << '=' << endl;
   }

   return 0;
}