1) accepted:
#include <string>
#include <iostream>
#include <vector>
#include <cstdio>
using namespace std;

typedef vector<string> vs;

int main()
{
   //freopen("272.txt", "r", stdin);

   string res;
   bool isFirst = true;
   char c = cin.get();
   while (!cin.eof()) {
      if (c != '"') res.push_back(c);
      else {
         if (isFirst) {
            res.push_back('`');
            res.push_back('`');
         }
         else {
            res.push_back('\'');
            res.push_back('\'');
         }
         isFirst = !isFirst;
      }
      c = cin.get();
   }

   cout << res;

   return 0;
}

________________________________________
0) Bad, outputs an extra line:
#include <string>
#include <iostream>
#include <vector>
#include <cstdio>
using namespace std;

typedef vector<string> vs;
typedef vector<int> vi;

int main()
{
   freopen("272.txt", "r", stdin);
   vs v;
   string line;
   while (!cin.eof()) {
      getline(cin, line);
      v.push_back(line);
   }

   bool isFirst = true;
   vs res;
   for (auto & s : v) {
      string resLine;
      if (!s.empty()) {
         for (size_t i = 0; i < s.size(); ++i) {
            const char c = s[i];
            if (c != '"') {
               resLine.push_back(c);
            }
            else {
               if (isFirst) {
                  resLine.push_back('`');
                  resLine.push_back('`');
               }
               else {
                  resLine.push_back('\'');
                  resLine.push_back('\'');
               }
               isFirst = !isFirst;
            }
         }
         res.push_back(resLine);
      }
   }

   for (auto & s : res) {
      cout << s << endl;
   }

   return 0;
}