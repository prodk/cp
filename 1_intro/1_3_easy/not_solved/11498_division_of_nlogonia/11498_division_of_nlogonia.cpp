1) 

0) wrong answer:
#include <iostream>
#include <cstdio>
using namespace std;

int main()
{
   freopen("11498.txt", "r", stdin);

   int k = 0;
   while (scanf("%d", &k), k != 0) {
      if (k > 0) {
         int x0 = 0, y0 = 0;
         scanf("%d %d", &x0, &y0);
         for (int i = 0; i < k; ++i) {
            int x = 0; int y = 0;
            scanf("%d %d", &x, &y);
            if (x == x0 || y == y0) printf("divisia\n");
            else if (x > x0 && y > y0) printf("NE\n");
            else if (x < x0 && y < y0) printf("SO\n");
            else if (x > x0 && y < y0) printf("SE\n");
            else if (x < x0 && y > y0) printf("NO\n");
         }
      }
   }

   return 0;
}