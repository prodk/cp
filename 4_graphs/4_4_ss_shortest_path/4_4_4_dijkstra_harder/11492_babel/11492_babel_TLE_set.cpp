#include <vector>
#include <iostream>
#include <cstdio>
#include <map>
#include <queue>
#include <functional>
#include <climits>
#include <algorithm>
#include <string>
#include <set>
using namespace std;

typedef pair<int, int> pii;

int m;
vector<int> dist;

int main()
{
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);

    while (cin >> m, m) {
        map<string, int> lang;
        string sLang, eLang;
        cin >> sLang >> eLang;

        if (lang.find(sLang) == end(lang)) {
            lang[sLang] = lang.size();
        }
        if (lang.find(eLang) == end(lang)) {
            lang[eLang] = lang.size();
        }

        map<int, pii> word;
        map<string, int> wordId;
        for (int i = 0; i < m; ++i) {
            string s0, s1, w;
            cin >> s0 >> s1 >> w;
            if (lang.find(s0) == end(lang)) {
                lang[s0] = lang.size();
            }
            if (lang.find(s1) == end(lang)) {
                lang[s1] = lang.size();
            }

            wordId[w] = i + 1; // id == 0 reserved for the start vertex
            word[wordId[w]] = pii(lang[s0], lang[s1]);
        }

        vector<set<pii>> g(m + 2); // g[0] and g[m+1] are reserved for the start/end vertices
        for (auto iter : wordId) {
            const int id = iter.second;
            const int lang0 = word[id].first;
            const int lang1 = word[id].second;
            if (lang0 == lang[sLang] || lang1 == lang[sLang]) {
                g[0].insert(pii(id, iter.first.size()));
                g[id].insert(pii(0, 0));
            }

            if (lang0 == lang[eLang] || lang1 == lang[eLang]) {
                g[m + 1].insert(pii(id, iter.first.size()));
                g[id].insert(pii(m + 1, 0));
            }

            for (auto otherIter : wordId) {
                const int oId = otherIter.second;
                if (id == oId) continue;

                const int oLang0 = word[oId].first;
                const int oLang1 = word[oId].second;
                if (lang0 == oLang0 || lang0 == oLang1 || lang1 == oLang0 || lang1 == oLang1) {
                    if (iter.first[0] != otherIter.first[0]) {
                        g[id].insert(pii(oId, otherIter.first.size()));
                        g[oId].insert(pii(id, iter.first.size()));
                    }
                }
            }
        }

        dist.clear();
        dist.resize(m + 2, INT_MAX);

        priority_queue<pii, vector<pii>, greater<pii>> pq;
        pq.push(pii(0, 0));
        dist[0] = 0;

        while (!pq.empty()) {
            const int u = pq.top().first;
            const int d = pq.top().second;
            pq.pop();
            if (d > dist[u]) continue;

            for (auto pv : g[u]) {
                const int v = pv.first;
                const int newDist = dist[u] + pv.second;
                if (newDist < dist[v]) {
                    dist[v] = newDist;
                    pq.push(pii(v, newDist));
                }
            }
        }

        if (dist[m + 1] == INT_MAX) cout << "impossivel" << endl;
        else cout << dist[m + 1] << endl;
    }

    return 0;
}