#include <vector>
#include <iostream>
#include <cstdio>
#include <map>
#include <unordered_map>
#include <queue>
#include <functional>
#include <climits>
#include <algorithm>
#include <string>
#include <set>
using namespace std;

typedef pair<int, int> pii;

int m;
vector<int> dist;
vector<set<pii>> g;
vector<pii> wordLang;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);

    g.resize(2002);
    wordLang.resize(2002);
    while (cin >> m, m) {
        map<string, int> lang;
        string sLang, eLang;
        cin >> sLang >> eLang;

        if (lang.find(sLang) == end(lang)) {
            lang[sLang] = lang.size();
        }
        if (lang.find(eLang) == end(lang)) {
            lang[eLang] = lang.size();
        }

        fill(begin(wordLang), end(wordLang), pii());
        unordered_map<int, string> word;
        for (int i = 0; i < m; ++i) {
            string s0, s1, w;
            cin >> s0 >> s1 >> w;
            if (lang.find(s0) == end(lang)) {
                lang[s0] = lang.size();
            }
            if (lang.find(s1) == end(lang)) {
                lang[s1] = lang.size();
            }

            const int id = i + 1; // id == 0 reserved for the start vertex
            word[id] = w;
            wordLang[id] = pii(lang[s0], lang[s1]);
        }

        fill(begin(g), end(g), set<pii>());
        const int endWordId = m + 1;
        for (int id = 1; id < endWordId; ++id) {
            const int lang0 = wordLang[id].first;
            const int lang1 = wordLang[id].second;
            if (lang0 == lang[sLang] || lang1 == lang[sLang]) {
                g[0].insert(pii(id, word[id].size()));
                g[id].insert(pii(0, 0));
            }

            if (lang0 == lang[eLang] || lang1 == lang[eLang]) {
                g[m + 1].insert(pii(id, word[id].size()));
                g[id].insert(pii(m + 1, 0));
            }

            for (int j = id + 1; j < endWordId; ++j) {
                const int oLang0 = wordLang[j].first;
                const int oLang1 = wordLang[j].second;
                if (lang0 == oLang0 || lang0 == oLang1 || lang1 == oLang0 || lang1 == oLang1) {
                    if (word[id][0] != word[j][0]) {
                        g[id].insert(pii(j, word[j].size()));
                        g[j].insert(pii(id, word[id].size()));
                    }
                }
            }
        }

        dist.clear();
        dist.resize(m + 2, INT_MAX);

        priority_queue<pii, vector<pii>, greater<pii>> pq;
        pq.push(pii(0, 0));
        dist[0] = 0;

        while (!pq.empty()) {
            const int u = pq.top().first;
            const int d = pq.top().second;
            pq.pop();
            if (d > dist[u]) continue;

            for (auto pv : g[u]) {
                const int v = pv.first;
                const int newDist = dist[u] + pv.second;
                if (newDist < dist[v]) {
                    dist[v] = newDist;
                    pq.push(pii(v, newDist));
                }
            }
        }

        if (dist[m + 1] == INT_MAX) cout << "impossivel" << endl;
        else cout << dist[m + 1] << endl;
    }

    return 0;
}