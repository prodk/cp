#include <iostream>
#include <queue>
#include <algorithm>
#include <cstdio>
#include <functional>
#include <climits>
#include <vector>
#include <sstream>
using namespace std;

typedef pair<int, int> pii;
typedef pair<int, pii> tNode; // first - vertex id, second.first - time to get to this verted, second.second - lift id

const int waiting_time = 60;
const int NUM_OF_FLOORS = 100;

int n, k;
vector<int> dist; // first - distance, second - lift id that was used to reach this distance
vector<int> id;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    vector<int> vertex(101);

    string line;
    while (getline(cin, line), !line.empty()) {
        stringstream ss(line);
        ss >> n;
        ss >> k;

        getline(cin, line);
        stringstream ss0(line);
        vector<int> t(n);
        for (int i = 0; i < n; ++i) {
            ss0 >> t[i];
        }

        vector<vector<tNode>> g(NUM_OF_FLOORS);
        for (int u = 0; u < n; ++u) {
            string line;
            getline(cin, line);
            stringstream ss(line);
            int prevVertex = -1;
            int v = 0;
            while (ss >> vertex[v++]);

            --v;
            // Important: connect every vertex with every other vertex. Otherwise will get wrong result for parallel edges
            for (int i = 0; i < v; ++i) {
                for (int j = i + 1; j < v; ++j) {
                    const int weight = abs(vertex[j] - vertex[i]) * t[u];
                    tNode node;
                    node.second = pii(weight, u);
                    node.first = vertex[j];

                    g[vertex[i]].push_back(node);
                    node.first = vertex[i];
                    g[vertex[j]].push_back(node);
                }
            }
        }

        dist.clear();
        dist.resize(NUM_OF_FLOORS, -1);
        id.clear();
        id.resize(NUM_OF_FLOORS, -1);

        priority_queue<pii, vector<pii>, greater<pii>> pq;
        pq.push(pii(0, 0));
        dist[0] = 0;

        while (!pq.empty()) {
            const pii pu = pq.top();
            pq.pop();
            const int u = pu.first;
            const int d = pu.second;
            if (d > dist[u]) continue;

            for (const auto & nv : g[u]) {
                const int v = nv.first;
                const int curLiftId = nv.second.second;
                const int prevLiftId = id[u];
                int newDist = dist[u] + abs(v - u)*t[curLiftId];

                // Keeping liftId is in principle redundant, because when we visit a lift 
                // that has dist == -1, it means that it is on the same lift id.
                // If we visit a lift with dist != -1, it means that we need to make a transfer
                if (prevLiftId != -1 && prevLiftId != curLiftId) { // if the lift has to be changed, add 60 seconds
                    newDist += waiting_time;
                }

                if (dist[v] == -1 || newDist < dist[v]) {
                    dist[v] = newDist;
                    id[v] = curLiftId;
                    pq.push(pii(v, newDist));
                }
            }
        }

        if (dist[k] == -1) cout << "IMPOSSIBLE" << endl;
        else cout << dist[k] << endl;
        
    }

    return 0;
}
