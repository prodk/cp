#include <iostream>
#include <queue>
#include <algorithm>
#include <cstdio>
#include <functional>
#include <climits>
#include <vector>
#include <sstream>
using namespace std;

typedef pair<int, int> pii;
typedef pair<int, pii> tNode;

const int waiting_time = 60;
const int NUM_OF_FLOORS = 100;

int n, k;
vector<pii> dist; // first - distance, second - lift id that was used to reach this distance

int main()
{
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);

    string line;
    while (getline(cin, line), !line.empty()) {
        stringstream ss(line);
        ss >> n;
        ss >> k;

        getline(cin, line);
        stringstream ss0(line);
        vector<int> t(n);
        for (int i = 0; i < n; ++i) {
            ss0 >> t[i];
        }

        vector<vector<tNode>> g(NUM_OF_FLOORS);
        for (int u = 0; u < n; ++u) {
            string line;
            getline(cin, line);
            stringstream ss(line);
            int prevVertex = -1;
            int v = 0;
            while (ss >> v) {
                if (prevVertex != -1) {
                    const int weight = (v - prevVertex) * t[u];
                    tNode node;
                    node.first = v;
                    node.second = pii(weight, u);
                    g[prevVertex].push_back(node);

                    node.first = prevVertex;
                    node.second = pii(weight, u);
                    g[v].push_back(node);
                }
                prevVertex = v;
            }
        }

        dist.clear();
        dist.resize(NUM_OF_FLOORS, pii(INT_MAX, -1));

        priority_queue<pii, vector<pii>, greater<pii>> pq;
        pq.push(pii(k, 0));
        dist[k] = pii(0, -1);

        while (!pq.empty()) {
            const pii pu = pq.top();
            pq.pop();
            const int u = pu.first;
            const int d = pu.second;
            if (d > dist[u].first) continue;

            for (const auto & nv : g[u]) {
                const int v = nv.first;
                int weight = nv.second.first;
                const int curLiftId = nv.second.second;
                const int prevLiftId = dist[u].second;
                if (prevLiftId != -1 && prevLiftId != curLiftId) { // if the lift has to be changed, ad 60 second
                    weight += waiting_time;
                }

                const int newDist = dist[u].first + weight;
                if (newDist < dist[v].first) {
                    dist[v].first = newDist;
                    dist[v].second = curLiftId;
                    pq.push(pii(v, newDist));
                }
            }
        }

        if (dist[0].first == INT_MAX) cout << "IMPOSSIBLE" << endl;
        else cout << dist[0].first << endl;
    }

    return 0;
}