#include <iostream>
#include <string>
#include <vector>
#include <cstdio>
#include <map>
#include <climits>
#include <queue>
#include <sstream>
using namespace std;

vector<vector<int>> g;
vector<int> dist;
int n;

bool differByOneLetter(const string & a, const string & b)
{
    const int len = (int)a.size();
    if (len != (int)b.size()) {
        return false;
    }

    int diff = 0;
    for (int i = 0; i < len; ++i) {
        if (a[i] != b[i]) {
            ++diff;
        }
    }

    return diff == 1;
}

int bfs(int u, int end)
{
    queue<int> q;
    q.push(u);
    dist[u] = 0;

    while (!q.empty()) {
        const int v = q.front();
        q.pop();
        for (auto w : g[v]) {
            if (dist[w] == INT_MAX) {
                dist[w] = dist[v] + 1;
                if (w == end) {
                    return dist[w];
                }
                q.push(w);
            }
        }
    }

    return 0;
}

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);

    int t = 0;
    cin >> t;
    cin.ignore();
    cin.ignore();
    while (t--)
    {
        n = 0;
        map<string, int> id;
        vector<string> words[11];

        string line;
        while (getline(cin, line) && line != "*") {
            words[line.size()].push_back(line);
            id[line] = n;
            ++n;
        }

        g.clear();
        g.resize(n);
        for (const auto & vs : words) {
            const int len = (int)vs.size();
            for (int i = 0; i < len - 1; ++i) {
                for (int j = i + 1; j < len; ++j) {
                    if (differByOneLetter(vs[i], vs[j])) {
                        const int u = id[vs[i]];
                        const int v = id[vs[j]];
                        g[u].push_back(v);
                        g[v].push_back(u);
                    }
                }
            }
        }

        string start, end;
        while (getline(cin, start) && start.size() > 0 && start[0] != '\n') {
            stringstream ss(start);
            ss >> start;
            ss >> end;
            if (start.empty() || end.empty()) {
                break;
            }

            dist.clear();
            dist.resize(n, INT_MAX);
            cout << start << " " << end << " " << bfs(id[start], id[end]) << endl;
        }
        if (t) cout << endl;
    }

    return 0;
}