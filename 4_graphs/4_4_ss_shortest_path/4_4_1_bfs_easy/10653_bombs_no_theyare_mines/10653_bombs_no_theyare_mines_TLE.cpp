#include <iostream>
#include <cstdio>
#include <vector>
#include <queue>
#include <climits>
#include <map>
using namespace std;

typedef pair<int, int> pii;

int nr, nc;
map<pii, int> dist;
int dr[] = { 1,  0, -1, 0 };
int dc[] = { 0, -1,  0, 1 };

const int OK = 0;
const int BOMB = 1;

bool isValid(const pii & p, const vector<vector<int>> & g) {
    return (p.first >= 0 && p.first < nr && p.second >= 0 && p.second < nc && g[p.first][p.second] != BOMB);
}

int main()
{
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);

    while (cin >> nr >> nc, (nr > 0 || nc > 0)) {
        vector<vector<int>> g;
        for (int r = 0; r < nr; ++r) {
            vector<int> row(nc, OK);
            g.push_back(row);
        }

        int n = 0;
        cin >> n;
        for (int i = 0; i < n; ++i) {
            int rowId = 0;
            cin >> rowId;
            int nBombs = 0;
            cin >> nBombs;
            for (int cnt = 0; cnt < nBombs; ++cnt) {
                int column = 0;
                cin >> column;
                g[rowId][column] = BOMB;
            }
        }

        pii start;
        cin >> start.first >> start.second;
        pii dest;
        cin >> dest.first >> dest.second;

        dist.clear();

        queue<pii> q;
        q.push(start);
        dist[start] = 0;

        while (!q.empty()) {
            const pii u = q.front();
            for (int i = 0; i < 4; ++i) {
                const pii v(u.first + dr[i], u.second + dc[i]);
                if (isValid(v, g)) {
                    if (dist.find(v) == dist.end()) {
                        dist[v] = dist[u] + 1;
                        q.push(v);
                    }
                }
            }
        }

        int res = 0;
        for (auto & p : dist) {
            if (p.second > res) {
                res = p.second;
            }
        }

        cout << res << endl;
    }

    return 0;
}