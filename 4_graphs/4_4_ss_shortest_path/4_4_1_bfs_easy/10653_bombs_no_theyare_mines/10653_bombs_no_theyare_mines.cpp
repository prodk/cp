#include <iostream>
#include <cstdio>
#include <vector>
#include <queue>
#include <climits>
using namespace std;

typedef pair<int, int> pii;

int nr, nc;
int dr[] = { 1,  0, -1, 0 };
int dc[] = { 0, -1,  0, 1 };
const int SIZE = 1004;
int g[SIZE][SIZE];

bool isValid(const pii & p) {
    return (p.first >= 0 && p.first < nr && p.second >= 0 && p.second < nc && g[p.first][p.second] == -1);
}

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);

    while (cin >> nr >> nc, (nr > 0 || nc > 0)) {
        for (int i = 0; i < SIZE; ++i) {
            fill(begin(g[i]), end(g[i]), -1);
        }

        int n = 0;
        cin >> n;
        for (int i = 0; i < n; ++i) {
            int rowId = 0;
            cin >> rowId;
            int nBombs = 0;
            cin >> nBombs;
            for (int cnt = 0; cnt < nBombs; ++cnt) {
                int column = 0;
                cin >> column;
                g[rowId][column] = INT_MAX;
            }
        }

        pii start;
        cin >> start.first >> start.second;
        pii dest;
        cin >> dest.first >> dest.second;

        queue<pii> q;
        q.push(start);

        int res = 0;
        while (!q.empty()) {
            const pii u = q.front();
            q.pop();
            for (int i = 0; i < 4; ++i) {
                const pii v(u.first + dr[i], u.second + dc[i]);
                if (isValid(v)) {
                    g[v.first][v.second] = g[u.first][u.second] + 1;
                    q.push(v);
                }
            }
        }

        cout << g[dest.first][dest.second] + 1 << endl;
    }

    return 0;
}