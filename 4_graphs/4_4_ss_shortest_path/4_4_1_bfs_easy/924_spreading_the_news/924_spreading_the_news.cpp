#include <iostream>
#include <cstdio>
#include <vector>
#include <map>
#include <climits>
#include <queue>
using namespace std;

vector<vector<int>> g;
vector<int> dist;
map<int, int> m;
int e;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);

    cin >> e;
    g.clear();
    g.resize(e);

    for (int i = 0; i < e; ++i) {
        int n = 0;
        cin >> n;
        for (int j = 0; j < n; ++j) {
            int v = 0;
            cin >> v;
            g[i].push_back(v);
        }
    }

    int t = 0;
    cin >> t;
    while (t--) {
        int start = 0;
        cin >> start;
        m.clear();
        dist.clear();
        dist.resize(e, INT_MAX);

        queue<int> q;
        q.push(start);
        dist[start] = 0;
        m[dist[start]]++;

        while (!q.empty()) {
            const int u = q.front();
            q.pop();
            for (auto v : g[u]) {
                if (dist[v] == INT_MAX) {
                    dist[v] = dist[u] + 1;
                    m[dist[v]]++;
                    q.push(v);
                }
            }
        }

        int mRes = 0;
        int dRes = 0;
        for (auto it : m) {
            if (mRes < it.second || (mRes == it.second && dRes == 0)) {
                mRes = it.second;
                dRes = it.first;
            }
        }

        if (dRes == 0) cout << "0" << endl;
        else cout << mRes << " " << dRes << endl;
    }

    return 0;
}