#include <vector>
#include <queue>
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <climits>
#include <functional>
using namespace std;

typedef pair<int, int> pii;

int testCnt, n, m, s, t, u, v, w;
vector<int> dist;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    cin >> testCnt;
    int curTest = 0;
    while (testCnt--) {
        ++curTest;
        cin >> n >> m >> s >> t;
        vector<vector<pii>> g(n);

        for (int i = 0; i < m; ++i) {
            cin >> u >> v >> w;
            g[u].push_back(pii(v, w));
            g[v].push_back(pii(u, w));
        }

        dist.clear();
        dist.resize(n, INT_MAX);

        priority_queue<pii, vector<pii>, greater<pii>> pq;
        pq.push(pii(s, 0));
        dist[s] = 0;
        while (!pq.empty()) {
            const pii pu = pq.top();
            pq.pop();
            const int d = pu.second;
            u = pu.first;
            if (d > dist[u]) continue;
            for (const auto & pv : g[u]) {
                v = pv.first;
                const int newDist = dist[u] + pv.second;
                if (newDist < dist[v]) {
                    dist[v] = newDist;
                    pq.push(pii(v, newDist));
                }
            }
        }

        cout << "Case #" << curTest << ": ";
        if (dist[t] == INT_MAX) cout << "unreachable" << endl;
        else cout << dist[t] << endl;
    }

    return 0;
}