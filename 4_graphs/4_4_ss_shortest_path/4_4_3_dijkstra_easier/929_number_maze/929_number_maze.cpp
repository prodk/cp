#include <vector>
#include <iostream>
#include <cstdio>
#include <queue>
#include <map>
#include <algorithm>
#include <cstring>
#include <climits>
using namespace std;

typedef pair<int, int> pii;
typedef pair<int, pii> node;

const int dr[] = { 1, 0, 0, -1 };
const int dc[] = { 0, 1, -1, 0 };
int n, m;
const int SIZE = 1000;
int dist[SIZE][SIZE];

bool isValid(const pii & p)
{
    const int r = p.first;
    const int c = p.second;
    return r >= 0 && r < n && c >= 0 && c < m;
}

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);

    int t = 0;
    cin >> t;
    while (t--) {
        cin >> n >> m;
        vector<vector<int>> v(n);
        for (int i = 0; i < n; ++i) {
            v[i].resize(m, 0);
            for (int j = 0; j < m; ++j)
                cin >> v[i][j];
        }

        for (int i = 0; i < SIZE; ++i) {
            fill(begin(dist[i]), end(dist[i]), INT_MAX);
        }

        auto comp = [](const node & lhs, const node & rhs) {
            bool result = (lhs.first > rhs.first);
            if (!result && lhs.first == rhs.first) {
                result = lhs.second.first > rhs.second.first;
                if (!result && lhs.second.first == rhs.second.first) {
                    result = lhs.second.second > rhs.second.second;
                }
            }
            return result;
        };

        priority_queue < node, vector<node>, decltype(comp)> pq(comp);

        pii start(0, 0);
        node startNode;
        startNode.first = 0;
        startNode.second = start;
        pq.push(startNode);
        dist[0][0] = v[0][0];
        while (!pq.empty()) {
            const node nd = pq.top();
            pq.pop();

            const pii& u = nd.second;
            const int d = nd.first;
            if (d > dist[u.first][u.second]) continue;

            for (int i = 0; i < 4; ++i) {
                const pii w(u.first + dr[i], u.second + dc[i]);
                if (isValid(w)) {
                    const int newDist = dist[u.first][u.second] + v[w.first][w.second];
                    if (newDist < dist[w.first][w.second]) {
                        dist[w.first][w.second] = newDist;
                        pq.push(make_pair(newDist, w));
                    }
                }
            }
        }

        cout << dist[n - 1][m - 1] << endl;
    }

    return 0;
}