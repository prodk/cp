#include <queue>
#include <vector>
#include <iostream>
#include <cstdio>
#include <algorithm>
#include <climits>
#include <functional>
using namespace std;

typedef pair<int, int> pii;
int testCnt, n, e, t, m;

vector<int> dist;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    cin >> testCnt;
    cin.ignore();
    while (testCnt--) {
        cin.ignore();
        cin >> n >> e >> t >> m;
        vector<vector<pii>> g;
        g.resize(n);
        for (int i = 0; i < m; ++i) {
            int u = 0;
            int v = 0;
            int weight = 0;
            cin >> u >> v >> weight;
            g[u - 1].push_back(make_pair(v-1, weight));
        }

        dist.resize(n);
        const int endVertex = e - 1;
        int res = 0;
        for (int start = 0; start < n; ++start) {
            fill(begin(dist), end(dist), INT_MAX);

            // run dijkstra from vertex start
            priority_queue<pii, vector<pii>, greater<pii>> pq;
            pq.push(make_pair(start, 0));
            dist[start] = 0;

            while (!pq.empty()) {
                const int u = pq.top().first;
                const int d = pq.top().second;
                pq.pop();
                if (d > dist[u]) continue;
                for (const auto & pv : g[u]) {
                    const int v = pv.first;
                    const int newDist = dist[u] + pv.second;
                    if (newDist < dist[v]) {
                        dist[v] = newDist;
                        pq.push(make_pair(v, newDist));
                    }
                }
            }


            if (dist[endVertex] <= t || start == endVertex) {
                ++res;
            }
        }

        cout << res << endl;
        if (testCnt) cout << endl;
    }

    return 0;
}
