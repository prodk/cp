#include <cstdio>
#include <vector>
#include <iostream>
#include <queue>
#include <algorithm>
#include <climits>
using namespace std;

int l, u, r;
const int MAX_SIZE = 1e4;
int dist[MAX_SIZE];

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    int caseCount = 0;
    while (cin >> l >> u >> r, r) {
        ++caseCount;
        fill(begin(dist), end(dist), INT_MAX);

        vector<int> v(r);
        for (int i = 0; i < r; ++i) {
            cin >> v[i];
        }

        int res = INT_MAX;
        queue<int> q;
        q.push(l);
        dist[l] = 0;
        while (!q.empty()) {
            const int cur = q.front();
            q.pop();

            if (cur == u) {
                res = dist[cur];
                break;
            }

            for (auto i : v) {
                const int newVal = (cur + i) % 10000;
                if (dist[newVal] != INT_MAX) continue;
                dist[newVal] = dist[cur] + 1;
                q.push(newVal);
            }
        }

        cout << "Case " << caseCount << ": ";
        if (res == INT_MAX) cout << "Permanently Locked" << endl;
        else cout << res << endl;
    }

    return 0;
}