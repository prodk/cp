#include <vector>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <climits>
using namespace std;

int p0, p1;
typedef pair<int, int> pii;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);

    while (!cin.eof()) {
        cin >> p0;
        if (p0 <= 0) break;
        vector<pii> m0(p0);
        for (int i = 0; i < p0; ++i) {
            cin >> m0[i].first >> m0[i].second;
        }

        cin >> p1;
        vector<pii> m1(p1);
        for (int i = 0; i < p1; ++i) {
            cin >> m1[i].first >> m1[i].second;
        }

        int res = INT_MAX;
        for (int i = 0; i < p0; ++i) {
            for (int j = 0; j < p1; ++j) {
                const int dx = abs(m0[i].first - m1[j].first);
                const int dy = abs(m0[i].second - m1[j].second);
                res = min(res, dx + dy);
            }
        }

        cout << res << endl;
    }

    return 0;
}