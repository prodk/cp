#include <vector>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <climits>
#include <queue>
#include <cstring>
using namespace std;

int p0, p1;
int nc, nr;
typedef pair<int, int> pii;
const int SIZE = 2048;
bool g[SIZE][SIZE];
bool visited[SIZE][SIZE];
int dr[] = { 1, 0, -1, 0 };
int dc[] = { 0, 1, 0, -1 };

bool isValid(const pii& p)
{
    const int r = p.first;
    const int c = p.second;
    return (r >= 0 && r < nr && c >= 0 && c < nc && !visited[r][c]);
}

int bfs(queue<pair<pii, int>> & q) {
    while (!q.empty()) {
        const pair<pii, int> node = q.front();
        const pii u = node.first;
        q.pop();
        for (int i = 0; i < 4; ++i) {
            const pii pNew(u.first + dr[i], u.second + dc[i]);
            if (isValid(pNew)) {
                visited[pNew.first][pNew.second] = true;
                const int newDist = node.second + 1;
                if (g[pNew.first][pNew.second]) return newDist;
                q.push(make_pair(pNew, newDist));
            }
        }
    }

    return -1;
}

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);

    while (!cin.eof()) {
        cin >> p0;
        if (p0 <= 0) break;

        memset(visited, false, sizeof(visited));
        memset(g, false, sizeof(g));

        queue<pair<pii, int>> q;
        nc = 0;
        nr = 0;
        for (int i = 0; i < p0; ++i) {
            pair<pii, int> node;
            cin >> node.first.first >> node.first.second;
            q.push(node);
            nr = max(nr, node.first.first);
            nc = max(nc, node.first.second);
        }

        cin >> p1;
        for (int i = 0; i < p1; ++i) {
            pair<pii, int> node;
            cin >> node.first.first >> node.first.second;
            g[node.first.first][node.first.second] = true;
            nr = max(nr, node.first.first);
            nc = max(nc, node.first.second);
        }
        ++nc;
        ++nr;

        cout << bfs(q) << endl;
    }

    return 0;
}