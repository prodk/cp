#include <vector>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <climits>
#include <queue>
using namespace std;

int p0, p1;
int nc, nr;
typedef pair<int, int> pii;
const int SIZE = 2048;
bool g[SIZE][SIZE];
bool visited[SIZE][SIZE];
int dr[] = { 1, 0, -1, 0 };
int dc[] = { 0, 1, 0, -1 };

bool isValid(const pii& p)
{
    const int r = p.first;
    const int c = p.second;
    return (r >= 0 && r < nr && c >= 0 && c < nc && !visited[r][c]);
}

int bfs(queue<pair<pii, int>> & q) {
    while (!q.empty()) {
        const pair<pii, int> node = q.front();
        const pii u = node.first;
        q.pop();
        visited[u.first][u.second] = true;
        for (int i = 0; i < 4; ++i) {
            const pii pNew(u.first + dr[i], u.second + dc[i]);
            if (isValid(pNew)) {
                const int newDist = node.second + 1;
                if (g[pNew.first][pNew.second]) return newDist;
                q.push(make_pair(pNew, newDist));
            }
        }
    }

    return -1;
}

int main()
{
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);

    while (!cin.eof()) {
        cin >> p0;
        if (p0 <= 0) break;

        for (int i = 0; i < SIZE; ++i) {
            fill(begin(visited[i]), end(visited[i]), false);
            fill(begin(g[i]), end(g[i]), false);
        }

        queue<pair<pii, int>> q;
        nc = 0;
        nr = 0;
        for (int i = 0; i < p0; ++i) {
            pii p;
            cin >> p.first >> p.second;
            q.push(make_pair(p, 0));
            nr = max(nr, p.first);
            nc = max(nc, p.second);
        }

        cin >> p1;
        vector<pii> m1;
        for (int i = 0; i < p1; ++i) {
            pii p;
            cin >> p.first >> p.second;
            g[p.first][p.second] = true;
            nr = max(nr, p.first);
            nc = max(nc, p.second);
        }
        ++nc;
        ++nr;

        cout << bfs(q) << endl;
    }

    return 0;
}