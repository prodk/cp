#include <iostream>
#include <cstdio>
#include <algorithm>
#include <vector>
#include <string>
#include <queue>
#include <limits>
using namespace std;

typedef pair<int, int> pii;

vector <vector <pii>> g;
vector<int> dist;
int n, m;

const int tLadder = 2;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    while (!cin.eof()) {
        string name;
        cin >> name;
        if (name.empty()) break;
        cin >> n >> m;
        const int len = n*m;
        g.clear();
        g.resize(len);
        dist.clear();
        dist.resize(len, numeric_limits<int>::max());
        for (int k = 0; k < n - 1; ++k) {
            for (int i = 0; i < m; ++i) {
                for (int j = 0; j < m; ++j) {
                    int t = 0;
                    cin >> t;
                    const int v = (k + 1)*m + j;
                    const int u = k * m + i;
                    g[u].push_back(pii(v, t + tLadder));
                }
            }
        }

        queue<pii> q;
        // Important: consider all the holes from the 1st floor
        for (int i = 0; i < m; i++) {
            q.push(pii(i, 0));
            dist[i] = 0;
        }
        while (!q.empty()) {
            const pii u = q.front();
            q.pop();
            for (auto v : g[u.first]) {
                const int newDist = dist[u.first] + v.second;
                if (newDist < dist[v.first]) {
                    dist[v.first] = newDist;
                    q.push(pii(v.first, newDist));
                }
            }
        }

        cout << name << endl;
        int res = numeric_limits<int>::max();
        for (int i = len - m; i < len; ++i) {
            res = min(res, dist[i]);
        }

        cout << res << endl;
    }

    return 0;
}