#include <string>
#include <sstream>
#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <limits>
#include <queue>
#include <functional>
using namespace std;

typedef pair<int, int> pii;

const int INF = numeric_limits<int>::max();
const int SIZE = 28;
int n;
vector<vector<pii>> g;
int outd[SIZE];
int dist[SIZE];

int main()
{
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);

    cin >> n;
    string line;
    cin.ignore();
    cin.ignore();
    while (n--) {
        g.clear();
        g.resize(SIZE);
        vector<pii> vStart;
        vector<int> vEnd;
        fill(begin(outd), end(outd), -1);
        while (getline(cin, line) && !line.empty()) {
            stringstream ss(line);
            char c = 'A';
            ss >> c;
            const int v = c - 'A';
            outd[v] = 0;
            int w = 0;
            ss >> w;
            pii p(v, -w);
            int cnt = 0;
            while (ss >> c) {
                const int u = c - 'A';
                g[u].push_back(p);
                outd[u] = (outd[u] == -1) ? 1 : ++outd[u];
                ++cnt;
            }
            if (0 == cnt) {
                vStart.push_back(p);
            }
        }

        for (int i = 0; i < SIZE; ++i) {
            if (outd[i] == 0) {
                vEnd.push_back(i);
            }
        }

        fill(begin(dist), end(dist), INF);
        priority_queue<pii, vector<pii>, greater<pii>> pq;
        for (auto p : vStart) {
            pq.push(p);
            dist[p.first] = p.second;
        }

        while (!pq.empty()) {
            const pii u = pq.top();
            pq.pop();
            for (auto v : g[u.first]) {
                const int newDist = dist[u.first] + v.second;
                if (dist[v.first] > newDist) {
                    dist[v.first] = newDist;
                    pq.push(v);
                }
            }
        }

        int res = INF;
        for (auto v : vEnd) {
            res = min(res, dist[v]);
        }
        cout << -1 * res << endl;

        if (n) cout << endl;
    }

    return 0;
}