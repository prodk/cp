#include <iostream>
#include <algorithm>
#include <string>
#include <cstdio>
using namespace std;

string name;
int t, nr, nc, r0, c0;
int g[100][100];
int memo[100][100];
const int dr[] = { 0, 1, -1, 0 };
const int dc[] = { 1, 0, 0, -1 };

int solve(int rCur, int cCur)
{
    const int hCur = g[rCur][cCur];
    int & res = memo[rCur][cCur];
    if (res != -1) return res;
    res = 0;
    for (int i = 0; i < 4; ++i) {
        const int r = rCur + dr[i];
        const int c = cCur + dc[i];
        if (r >= 0 && r < nr && c >= 0 && c < nc && g[r][c] < hCur) {
            res = max(res, 1 + solve(r, c));
        }
    }
    return res;
}

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    cin >> t;
    while (t--) {
        cin >> name;
        cin >> nr >> nc;

        int hmax = 0;
        for (int r = 0; r < nr; ++r) {
            for (int c = 0; c < nc; ++c) {
                cin >> g[r][c];
                if (g[r][c] > hmax) {
                    hmax = g[r][c];
                }
            }
        }

        for (int i = 0; i < 100; ++i)
            fill(begin(memo[i]), end(memo[i]), -1);

        int res = 0;
        for (int r = 0; r < nr; ++r) {
            for (int c = 0; c < nc; ++c) {
                if (memo[r][c] == -1) {
                    res = max(res, solve(r, c));
                }
            }
        }

        cout << name << ": " << 1 + res << endl;
    }

    return 0;
}