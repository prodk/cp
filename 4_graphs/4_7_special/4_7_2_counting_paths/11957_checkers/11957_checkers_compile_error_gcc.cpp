#include <iostream>
#include <vector>
#include <cstdio>
#include <string>
using namespace std;

typedef long long ll;

int t, n;

const int SIZE = 100;
ll numPath[SIZE][SIZE];
vector<string> v;

const ll modVal = 1000007;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    cin >> t;
    int testCnt = 0;
    while (t--) {
        ++testCnt;
        cin >> n;
        v.clear();
        v.resize(n);

        int xw = 0;
        int yw = 0;
        for (int i = 0; i < n; ++i) {
            const int y = i;
            cin >> v[y];
            auto iter = find(begin(v[y]), end(v[y]), 'W');
            if (iter != end(v[y])) {
                yw = y;
                xw = iter - begin(v[y]);
            }
        }

        fill_n(*numPath, SIZE*SIZE, 0);

        numPath[yw][xw] = 1;
        for (int y = yw - 1; y >= 0; --y) { // Start from the 'next' state, i.e. next top row
            for (int x = 0; x < n; ++x) {
                if (v[y][x] == 'B') continue;
                // right previous state
                const int yPrev = y + 1;
                const int xRight = x + 1;
                const int xLeft = x - 1;

                const int yPrev2 = y + 2;
                const int xRight2 = x + 2;
                const int xLeft2 = x - 2;
                if (yPrev < n) {
                    if (xRight < n && v[yPrev][xRight] != 'B') {
                        numPath[y][x] += numPath[yPrev][xRight];
                    }
                    if (xLeft >= 0 && v[yPrev][xLeft] != 'B') {
                        numPath[y][x] += numPath[yPrev][xLeft];
                    }

                    if (yPrev2 < n) {
                        if (xRight < n && v[yPrev][xRight] == 'B') {
                            if (xRight2 < n && v[yPrev2][xRight2] != 'B') {
                                numPath[y][x] += numPath[yPrev2][xRight2];
                            }
                        }
                        if (xLeft >= 0 && v[yPrev][xLeft] == 'B') {
                            if (xLeft2 >= 0 && v[yPrev2][xLeft2] != 'B') {
                                numPath[y][x] += numPath[yPrev2][xLeft2];
                            }
                        }
                    }
                }

                numPath[y][x] %= modVal;
            }
        }

        ll res = 0;
        for (int x = 0; x < n; ++x) {
            if (v[0][x] != 'B') {
                res += numPath[0][x];
                res %= modVal;
            }
        }

        cout << "Case " << testCnt << ": " << res << endl;
    }

    return 0;
}
