#include <string>
#include <vector>
#include <cstdlib>
#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cctype>
using namespace std;

int n;
typedef long long ll;
vector<vector<int>> rows;
ll memo[16][16];

int charToInt(const char c)
{
    if (isdigit(c))
        return c - '0';
    if (isalpha(c))
        return c - 'A' + 10;
    return -1;
}

bool isValid(const int prevRow, const int curRow)
{
    const int d = abs(curRow - prevRow);
    return d >= 2;
}

ll solve(int curNum, int r, int c)
{
    if (r >= n || c >= n) {
        memo[r][c] = 0;
        return 0;
    }

    if (memo[r][c] != -1) {
        return memo[r][c];
    }

    if (curNum == n - 1) {
        memo[r][c] = 1;
        return 1;
    }

    ll res = 0;
    const int nextCol = c + 1;
    if (nextCol < n) {
        for (auto nextRow : rows[nextCol]) {
            if (isValid(r, nextRow)) {
                res += solve(curNum + 1, nextRow, nextCol);
            }
        }
    }

    memo[r][c] = res;
    return res;
}

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    string line;
    while (cin >> line && !line.empty()) {
        n = (int)line.size();
        rows.clear();
        rows.resize(n);

        vector<int> allRows;
        for (int i = 0; i < n; ++i) {
            allRows.push_back(i);
        }

        for (int i = 0; i < n; ++i) {
            if (line[i] == '?') {
                rows[i] = allRows;
            }
            else {
                const int r = charToInt(line[i]) - 1;
                rows[i].push_back(r);
            }
        }

        for (int i = 0; i < 16; ++i) {
            fill(begin(memo[i]), end(memo[i]), -1);
        }

        ll res = 0;
        for (auto r : rows[0]) {
            res += solve(0, r, 0);
        }

        cout << res << endl;
    }

    return 0;
}