#include <iostream>
#include <vector>
#include <cstdio>
#include <limits>
#include <queue>
#include <algorithm>
using namespace std;

typedef pair<int, int> pii;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    bool firstTime = true;
    while (!cin.eof()) {
        int n = 0;
        cin >> n;
        if (n == 0) break;

        if (!firstTime) cout << endl;
        firstTime = false;

        vector<vector<int>> g(n);
        vector<int> outg(n, 0);
        for (int i = 0; i < n; ++i) {
            int m = 0;
            cin >> m;
            outg[i] = m;
            for (int j = 0; j < m; ++j) {
                int v = 0;
                cin >> v;
                g[i].push_back(v);
            }
        }

        vector<bool> visited(n, false);
        vector<int> numPath(n, 0);
        queue<int> q;
        q.push(0);
        numPath[0] = 1;
        visited[0] = true;

        while (!q.empty()) {
            const int u = q.front();
            q.pop();
            for (int v : g[u]) {
                if (!visited[v]) {
                    visited[v] = true;
                    q.push(v);
                }
                numPath[v] += numPath[u];
            }
        }

        int res = 0;
        for (int i = 0; i < n; ++i) {
            if (outg[i] == 0) {
                res += numPath[i];
            }
        }
        cout << res << endl;

        cin.ignore();
    }

    return 0;
}