#include <iostream>
#include <vector>
#include <cstdio>
#include <limits>
#include <queue>
#include <algorithm>
using namespace std;

typedef pair<int, int> pii;
int m, v;

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    bool firstTime = true;
    while (!cin.eof()) {
        int n = 0;
        cin >> n;
        if (n == 0) break;

        if (!firstTime) cout << endl;
        firstTime = false;

        vector<int> outg(n, 0);
        vector<int> numPath(n, 0);
        numPath[0] = 1;
        for (int i = 0; i < n; ++i) {
            cin >> m;
            outg[i] = m;
            for (int j = 0; j < m; ++j) {
                cin >> v;
                numPath[v] += numPath[i];
            }
        }

        int res = 0;
        for (int i = 0; i < n; ++i) {
            if (outg[i] == 0) {
                res += numPath[i];
            }
        }
        cout << res << endl;

        cin.ignore();
    }

    return 0;
}