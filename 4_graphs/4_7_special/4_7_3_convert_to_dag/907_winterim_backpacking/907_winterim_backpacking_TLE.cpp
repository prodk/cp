#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <set>
#include <limits>
using namespace std;

int n, k, numPoints;
vector<int> v;
vector<int> walkingDist;
set<int> maxDist;


int memo[602][302];

int solve(int id, int curNights, int prevDist)
{
    if (id == numPoints - 1) {
        memo[id][curNights] = numeric_limits<int>::max();
        if (curNights == 0) {
            walkingDist.push_back(prevDist);
            memo[id][curNights] = *max_element(begin(walkingDist), end(walkingDist));
            maxDist.insert(memo[id][curNights]);
            walkingDist.pop_back();
        }
        return memo[id][curNights];
    }

    if (memo[id][curNights] != -1) {
        return memo[id][curNights];
    }

    const int campPointsLeft = numPoints - id;
    if (curNights > campPointsLeft) {
        memo[id][curNights] = numeric_limits<int>::max();
        return memo[id][curNights];
    }

    const int nextId = id + 1;
    const int distToNextId = prevDist + v[nextId];
    const int d = solve(nextId, curNights, distToNextId);

    if (curNights > 0) {
        walkingDist.push_back(distToNextId);
        // min is redundant, memo is used just to see whether the state has been visited
        memo[id][curNights] = min(d, solve(nextId, curNights - 1, 0));
        walkingDist.pop_back();
    }

    return memo[id][curNights];
}

int main()
{
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);

    while (cin >> n >> k && (n || k)) {
        v.clear();
        numPoints = n + 2;
        v.resize(numPoints);
        for (int i = 1; i < numPoints; ++i) {
            cin >> v[i];
        }

        walkingDist.clear();
        maxDist.clear();
        fill_n(*memo, 602 * 302, -1);

        solve(0, k, 0);

        const int minOfMax = maxDist.empty() ? 0 : *begin(maxDist);

        cout << minOfMax << endl << std::flush;
    }

    return 0;
}