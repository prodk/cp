#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <set>
#include <limits>
using namespace std;

int n, k, numPoints;
vector<int> v;
int memo[602][302];

int solve(int id, int curNights)
{
    if (id == numPoints) {
        return 0;
    }
    if (curNights == 0) {
        int sumDist = 0;
        for (int i = id; i < numPoints; ++i) {
            sumDist += v[i];
        }

        return sumDist;
    }

    int & res = memo[id][curNights];
    if (res != -1) return res;

    res = numeric_limits<int>::max();
    int curDist = v[id];
    for (int nextId = id + 1; nextId <= numPoints && res > curDist; ++nextId) {
        res = min(res, max(curDist, solve(nextId, curNights - 1)));
        curDist += v[nextId];
    }

    return res;
}

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);

    while (cin >> n >> k && (n || k)) {
        v.clear();
        numPoints = n + 1;
        v.resize(numPoints + 1);
        for (int i = 0; i < numPoints; ++i) {
            cin >> v[i];
        }

        fill_n(*memo, 602 * 302, -1);

        cout << solve(0, k) << endl;
    }

    return 0;
}