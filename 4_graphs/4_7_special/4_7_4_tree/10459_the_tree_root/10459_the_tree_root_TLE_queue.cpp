#include <iostream>
#include <cstdio>
#include <vector>
#include <queue>
#include <limits>
#include <set>
using namespace std;

typedef pair<int, int> pii;

const int INF = 1e9;
const int SIZE = 5002;

int n;
int dist[SIZE], path[SIZE];
bool best[SIZE], worst[SIZE];

inline pii farthestPoint(int start, int* path, const vector<vector<int>>& g)
{
    for (int i = 0; i < n; ++i) dist[i] = INF;

    queue<int> q;
    q.push(start);
    int farthest = start;
    int dMax = 0;
    dist[start] = 0;
    while (!q.empty()) {
        const int u = q.front();
        q.pop();
        for (int i = 0; i < g[u].size(); ++i) {
            const int v = g[u][i];
            if (dist[v] == INF) {
                dist[v] = dist[u] + 1;
                q.push(v);
                if (dMax < dist[v]) {
                    dMax = dist[v];
                    farthest = v;
                    path[v] = u;
                }
            }
        }
    }

    for (int i = 0; i < n; ++i) {
        if (dist[i] == dMax) {
            worst[i] = true;
        }
    }

    return pii(farthest, dMax);
}

int main()
{
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);

    while (cin >> n) {
        vector<vector<int>> g(n);
        for (int u = 0; u < n; ++u) {
            int m = 0;
            cin >> m;
            for (int j = 0; j < m; ++j) {
                int v = 0;
                cin >> v;
                g[u].push_back(v - 1);
            }
        }

        for (int i = 0; i < n; ++i) {
            best[i] = false;
            worst[i] = false;
        }

        const pii pFarthest = farthestPoint(0, path, g);
        const pii diameter = farthestPoint(pFarthest.first, path, g);

        const int halfDiameter = diameter.second / 2;
        const bool isEven = (0 == diameter.second % 2);
        int dist = 0;
        if (isEven) {
            for (int u = diameter.first; u != pFarthest.first; u = path[u]) {
                if (dist == halfDiameter) {
                    best[u] = true;
                }
                ++dist;
            }
        }
        else {
            const int halfDiameter2 = halfDiameter + 1;
            for (int u = diameter.first; u != pFarthest.first; u = path[u]) {
                if (dist == halfDiameter || dist == halfDiameter2) {
                    best[u] = true;
                }
                ++dist;
            }
        }

        cout << "Best Roots  :";
        for (int i = 0; i < n; ++i) {
            if (best[i]) cout << " " << i + 1;
        }

        cout << endl;

        cout << "Worst Roots :";
        for (int i = 0; i < n; ++i) {
            if (worst[i]) cout << " " << i + 1;
        }
        cout << endl;
    }

    return 0;
}

