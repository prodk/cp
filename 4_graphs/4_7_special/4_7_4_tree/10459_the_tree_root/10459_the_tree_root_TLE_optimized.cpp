#include <iostream>
#include <cstdio>
#include <vector>
#include <queue>
#include <limits>
#include <set>
using namespace std;

typedef pair<int, int> pii;

int n;
vector<vector<int>> g;
set<int> best, worst;
const int INF = numeric_limits<int>::max();
const int SIZE = 5002;
int dist[SIZE], path[SIZE];

pii farthestPoint(int start, int* path)
{
    fill(begin(dist), end(dist), INF);
    queue<int> q;
    q.push(start);
    int farthest = start;
    int dMax = 0;
    dist[start] = 0;
    while (!q.empty()) {
        const int u = q.front();
        q.pop();
        for (auto v : g[u]) {
            if (dist[v] == INF) {
                dist[v] = dist[u] + 1;
                q.push(v);
                if (dMax < dist[v]) {
                    dMax = dist[v];
                    farthest = v;
                    path[v] = u;
                }
            }
        }
    }

    for (int i = 0; i < n; ++i) {
        if (dist[i] == dMax) {
            worst.insert(i);
        }
    }

    return pii(farthest, dMax);
}

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);

    while (cin >> n) {
        g.clear();
        g.resize(n);
        for (int u = 0; u < n; ++u) {
            int m = 0;
            cin >> m;
            for (int j = 0; j < m; ++j) {
                int v = 0;
                cin >> v;
                g[u].push_back(v - 1);
            }
        }

        best.clear();
        worst.clear();

        const pii pFarthest = farthestPoint(0, path);
        const pii diameter = farthestPoint(pFarthest.first, path);

        const int halfDiameter = diameter.second / 2;
        const bool isEven = (0 == diameter.second % 2);
        int dist = 0;
        if (isEven) {
            for (int u = diameter.first; u != pFarthest.first; u = path[u]) {
                if (dist == halfDiameter) {
                    best.insert(u);
                }
                ++dist;
            }
        }
        else {
            const int halfDiameter2 = halfDiameter + 1;
            for (int u = diameter.first; u != pFarthest.first; u = path[u]) {
                if (dist == halfDiameter || dist == halfDiameter2) {
                    best.insert(u);
                }
                ++dist;
            }
        }

        cout << "Best Roots  :";
        for (auto b : best) cout << " " << b + 1;
        cout << endl;

        cout << "Worst Roots :";
        for (auto w : worst) cout << " " << w + 1;
        cout << endl;
    }

    return 0;
}