#include <iostream>
#include <vector>
#include <cstdio>
#include <map>
#include <string>
#include <algorithm>
using namespace std;

size_t lessThan[20];
vector<size_t> state;
map<char, int> vars;
string v;
int n;

bool notChecked(size_t id, size_t checkedMask)
{
    const size_t curMask = 1 << id;
    const bool res = !(curMask & checkedMask);
    return res;
}

bool isValid(size_t id, size_t checkedMask)
{
    const size_t allSmallerValuesChecked = lessThan[id] & checkedMask;
    const bool res = lessThan[id] == allSmallerValuesChecked;
    return res;
}

bool solve(int curSize, size_t checkedMask) {
    if (n == curSize) {
        for (int i = 0; i < n; ++i) {
            const int c = state[i];
            cout << v[c];
            if (i != n - 1) cout << " ";
        }
        cout << endl;
        return true;
    }

    bool res = false;
    for (size_t id = 0; id < n; ++id) {
        if (notChecked(id, checkedMask) && isValid(id, checkedMask)) {
            state.push_back(id);
            const size_t newMask = checkedMask | 1 << id;
            res |= solve(curSize + 1, newMask);
            state.pop_back();
        }
    }

    return res;
}

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);

    int t = 0;
    cin >> t;
    cin.ignore();
    while (t--) {
        string line;
        getline(cin, line);
        getline(cin, line);
        v.clear();
        for (auto c : line) {
            if (c != ' ') v.push_back(c);
        }

        // sort the input to have output in alphabetical order
        sort(begin(v), end(v));

        vars.clear();
        int count = 0;
        for (auto c : v) {
            vars[c] = count;
            ++count;
        }

        n = (int)vars.size();

        fill(begin(lessThan), end(lessThan), 0);

        string cmd;
        getline(cin, cmd);
        for (int i = 0; i < cmd.size(); i += 4) {
            const int next = i + 2;
            if (next < cmd.size()) {
                lessThan[vars[cmd[next]]] |= 1 << vars[cmd[i]];
            }
        }

        state.clear();
        const bool atLeastOne = solve(0, 0);

        if (!atLeastOne) cout << "NO" << endl;
        if (t) cout << endl;
    }

    return 0;
}