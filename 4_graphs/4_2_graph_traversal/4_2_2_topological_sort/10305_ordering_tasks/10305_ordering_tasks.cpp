#include <iostream>
#include <cstdio>
#include <vector>
#include <stack>
using namespace std;

vector<vector<int>> g;
vector<bool> visited;
stack<int> res;

void dfs(const int u)
{
    visited[u] = true;
    for (auto v : g[u]) {
        if (!visited[v]) {
            dfs(v);
        }
    }
    res.push(u);
}

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);

    int n = 0;
    int m = 0;
    while (cin >> n >> m && (n > 0 || m > 0)) {
        g.clear();
        g.resize(n);
        visited.clear();
        visited.resize(n, false);
        for (int i = 0; i < m; ++i) {
            int u = 0;
            int v = 0;
            cin >> u >> v;
            g[u - 1].push_back(v - 1);
        }

        stack<int>().swap(res);
        for (int u = 0; u < n; ++u) {
            if (!visited[u]) {
                dfs(u);
            }
        }

        while (!res.empty()) {
            cout << res.top() + 1;
            if (res.size() != 1) cout << " ";
            res.pop();
        }
        cout << endl;
    }

    return 0;
}