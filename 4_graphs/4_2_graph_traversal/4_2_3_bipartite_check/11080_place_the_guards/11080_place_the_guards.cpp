#include <queue>
#include <iostream>
#include <cstdio>
#include <vector>
#include <climits>
using namespace std;

int n, m;
vector<vector<int>> g;
vector<int> color;
bool isBipartite;

int bfs(int u)
{
    if (color[u] != INT_MAX) {
        return 0;
    }
    int black = 0;
    int white = 0;
    queue<int> q;

    q.push(u);
    color[u] = 0;
    ++black;

    while (!q.empty() && isBipartite) {
        const int u = q.front();
        q.pop();
        for (auto v : g[u]) {
            if (color[v] == INT_MAX) {
                color[v] = 1 - color[u];
                q.push(v);
                (color[v] == 0) ? ++black : ++white;
            }
            else if (color[v] == color[u]) {
                isBipartite = false;
            }
        }

    }

    return max(1, min(black, white));
}

int main()
{
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);

    int t = 0;
    cin >> t;
    while (t--) {
        cin >> n;
        cin >> m;
        g.clear();
        g.resize(n);
        for (int i = 0; i < m; ++i) {
            int u = 0;
            int v = 0;
            cin >> u >> v;
            g[u].push_back(v);
            g[v].push_back(u);
        }
        color.clear();
        color.resize(n, INT_MAX);

        isBipartite = true;
        int res = 0;
        for (int u = 0; u < n && isBipartite; ++u) {
            res += bfs(u);
        }

        if (isBipartite) cout << res << endl;
        else cout << "-1" << endl;
    }

    return 0;
}