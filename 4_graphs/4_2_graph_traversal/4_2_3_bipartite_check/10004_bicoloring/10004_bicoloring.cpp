#include <queue>
#include <iostream>
#include <cstdio>
#include <vector>
#include <climits>
using namespace std;

int n, m;
vector<vector<int>> g;
vector<int> color;

bool bfs()
{
    bool res = true;
    queue<int> q;

    q.push(0);
    color[0] = 0;

    while (!q.empty() && res) {
        const int u = q.front();
        q.pop();
        for (auto v : g[u]) {
            if (color[v] == INT_MAX) {
                color[v] = 1 - color[u];
                q.push(v);
            }
            else if (color[v] == color[u]) {
                res = false;
            }
        }

    }

    return res;
}

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    while (cin >> n && n > 0) {
        cin >> m;
        g.clear();
        g.resize(n);
        for (int i = 0; i < m; ++i) {
            int u = 0;
            int v = 0;
            cin >> u >> v;
            g[u].push_back(v);
            g[v].push_back(u);
        }
        color.clear();
        color.resize(n, INT_MAX);

        if (bfs()) cout << "BICOLORABLE." << endl;
        else cout << "NOT BICOLORABLE." << endl;
    }

    return 0;
}