#include <iostream>
#include <vector>
#include <cstdio>
#include <climits>
#include <algorithm>
using namespace std;

vector<vector<int>> g;
vector<bool> visited;
vector<int> clusterSize;

int dfs(int u)
{
    visited[u] = true;
    int compSize = 1;
    for (auto w : g[u]) {
        if (!visited[w]) {
            compSize += dfs(w);
        }
    }

    return compSize;
}

int main()
{
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);

    int t = 0;
    cin >> t;
    int count = 0;
    while (t--) {
        ++count;
        int n = 0;
        cin >> n;

        g.clear();
        g.resize(n);
        visited.resize(n, false);
        clusterSize.clear();
        clusterSize.resize(n, 0);
        for (int i = 0; i < n; ++i) {
            int u = 0;
            int v = 0;
            cin >> u >> v;
            g[u - 1].push_back(v - 1);
        }

        for (int u = 0; u < n; ++u) {
            fill(begin(visited), end(visited), false);
            clusterSize[u] = dfs(u);
        }

        int res = 0;
        int maxSize = 0;
        for (int i = 0; i < n; ++i) {
            if (maxSize < clusterSize[i]) {
                res = i;
                maxSize = clusterSize[i];
            }
        }

        cout << "Case " << count << ": " << res+1 << endl;
    }

    return 0;
}