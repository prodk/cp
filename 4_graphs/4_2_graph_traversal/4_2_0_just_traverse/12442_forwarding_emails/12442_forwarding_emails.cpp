#include <iostream>
#include <vector>
#include <cstdio>
#include <climits>
#include <algorithm>
using namespace std;

vector<vector<int>> g;
vector<bool> visited;
vector<int> roots;
vector<int> clusterSize;

int dfs(int u, int & r)
{
    visited[u] = true;
    int compSize = 1;
    for (auto w : g[u]) {
        if (visited[w] && clusterSize[w] == 0) { // a cycle
            r = w;
        }
        else if (visited[w] && clusterSize[w] != 0) {
            compSize += clusterSize[roots[w]];
        }
        else if (!visited[w]) {
            int rNew = w;
            compSize += dfs(w, rNew);
            if (rNew != w) { // a cycle was encountered
                roots[w] = rNew;
                r = rNew;
            }
        }
    }

    clusterSize[u] = compSize;

    return clusterSize[u];
}

int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    int t = 0;
    cin >> t;
    int count = 0;
    while (t--) {
        ++count;
        int n = 0;
        cin >> n;

        g.clear();
        g.resize(n);
        visited.clear();
        visited.resize(n, false);
        roots.resize(n, -1);
        clusterSize.clear();
        clusterSize.resize(n, 0);
        for (int i = 0; i < n; ++i) {
            int u = 0;
            int v = 0;
            cin >> u >> v;
            g[u - 1].push_back(v - 1);
            roots[i] = i;
        }

        for (int u = 0; u < n; ++u) {
            if (!visited[u]) {
                clusterSize[u] = dfs(u, u);
            }
            else {
                clusterSize[u] = clusterSize[roots[u]];
            }
        }

        int res = 0;
        int maxSize = 0;
        for (int i = 0; i < n; ++i) {
            if (maxSize < clusterSize[i]) {
                res = i;
                maxSize = clusterSize[i];
            }
        }

        cout << "Case " << count << ": " << res + 1 << endl;
    }

    return 0;
}