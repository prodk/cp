#include <vector>
#include <string>
#include <iostream>
#include <cstdio>
#include <map>
using namespace std;

typedef pair<int, int> pii;

int N, M, S;
map<char, char> left, right;

char changeState(const char curState, const char cmd)
{
	char res = curState;
	if (cmd == 'D') {
		res = right[curState];
	}
	else if (cmd == 'E') {
		res = left[curState];
	}
	return res;
}

int main()
{
	freopen("in.txt", "r", stdin);
	freopen("out.txt", "w", stdout);
	ios_base::sync_with_stdio(0);
	
	map<char, pii> state;
	state['N'] = pii(-1, 0);
	state['S'] = pii(1, 0);
	state['L'] = pii(0, 1);
	state['O'] = pii(0, -1);
	
	right['N'] = 'L';
	left['N'] = 'O';
	right['S'] = 'O';
	left['S'] = 'L';
	right['L'] = 'S';
	left['L'] = 'N';
	right['O'] = 'N';
	left['O'] = 'S';
	
	cin >> N >> M >> S;
	
	vector<string> v;
	while ((N && M) || S) {
		for (int r = 0; r < N; ++r) {
			string row;
			cin >> row;
			v.push_back(row);
		}
		
		string cmd;
		cin >> cmd;

		pii curPos;
		char curState = 'N';
		bool found = false;
		for (int i = 0; i < N && !found; ++i) {
			for (int j = 0; j < v[i].size(); ++j) {
				if (c == 'N' || c == 'S' || c == 'L' || c == 'O') {
					curPos.first = i;
					curPos.second = j;
					curState = c;
					found = true;
					break;
				}
			}
		}
		
		int res = 0;
		for (auto c : cmd) {
			const char newState = changeState(curState, c);
			if (curState == newState) {
				const int newX = curPos.first + state[curState].first;
				const int newY = curPos.second + state[curState].second;
				if (newX >= 0 && newX < N && newY >= 0 && newY < M && v[newX][newY] != '#') {
					curPos.first = newX;
					curPos.second = newY;
					if (v[newX][newY] == '*') {
						++res;
						v[newX][newY] = '.';
					}
				}
			}
		}

		cout << res;
	}	
	
	return 0;
}