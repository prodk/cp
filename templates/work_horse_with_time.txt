#include <iostream>
#include <cstdio>   // printf, scanf, freopen
#include <vector>
#include <string>    // to_string, stoi, string::npos, getline(std::cin, line), find, rfind
#include <algorithm> // count, fill, unique, max_element, next_permutation, set_intersection
                     // nth_element, sort, lower_bound, binary_search
#include <cmath>     // hypot, abs(float), floor, ceil
#include <cstdlib>   // abs(int)
#include <climits>  // INT_MAX, INT_MIN, LONG_MIN, LLONG_MAX etc.
#include <sstream>
#include <set>
#include <map>
#include <functional> // greater<>
#include <queue>    // priority_queue
#include <locale>   // toupper, tolower
#include <cctype>   // isalnum, isdigit
#include <iomanip>  // setprecision
#include <cstring>  // memset, memcpy
#include <unordered_map>
#include <unordered_set>
#include <numeric>  // accumulate
#include <bitset>
#include <fstream>
#include <ctime>
#include <stack>
#include <iterator> // distance
#include <utility>  // make_pair
#include <regex>
using namespace std;

typedef vector<int> vi;
typedef vector<string> vs;
typedef pair<int, int> ii;
typedef vector<ii> vii;

#define DEBUG 1 // set to 0 before submit

int main()
{
#if DEBUG
   clock_t t = clock();
   freopen("in.txt", "r", stdin);
   //freopen("out.txt","w",stdout);
#endif



#if DEBUG
   t = clock() - t;
   printf("%f s\n", ((float)t) / CLOCKS_PER_SEC);
#endif

   return 0;
}